package de.qbert.wallpaper;

import android.os.Bundle;
import android.preference.PreferenceActivity;


public class WallpaperPreferencesActivity extends PreferenceActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		
		setResult(RESULT_OK);
	}
}
