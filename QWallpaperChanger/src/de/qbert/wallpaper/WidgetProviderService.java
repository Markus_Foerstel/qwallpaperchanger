package de.qbert.wallpaper;

import java.util.Random;

import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class WidgetProviderService extends Service {

	private static final String LOG_TAG = "Wallpaper Widget Service";

	public static final String SERVICE_ACTION_ID = "de.qbert.wallpaper.action_id";

	public static final String SERVICE_ACTION_UPDATENAME = "de.qbert.wallpaper.updateName";
	public static final String SERVICE_ACTION_FORWARD = "de.qbert.wallpaper.forward";
	public static final String SERVICE_ACTION_BACKWARD = "de.qbert.wallpaper.backward";
	public static final String SERVICE_ACTION_FAVORITE = "de.qbert.wallpaper.favorite";
	public static final String SERVICE_ACTION_UPDATE = "de.qbert.wallpaper.update";

	@Override
	public void onStart(Intent intent, int startId) {
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());

		String action = intent.getStringExtra(SERVICE_ACTION_ID);

		Log.d(LOG_TAG, "onStart() " + action);

		DocBase doc = DocBase.getInstance();
		if (doc.getLastError() != null){
	        Toast.makeText(this, doc.getLastError(), Toast.LENGTH_LONG).show();
		} else {
		
		WallpaperEntry entry = doc.getActiveWallpaper();		
		String title = "--- nothing found ----";
		if (entry != null)
			title = entry.getTitle();

		if (action.equals(SERVICE_ACTION_UPDATENAME)) {

			int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

			for (int widgetId : allWidgetIds) {
				RemoteViews remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.widget_layout);

				// Register onClickListener for update button
				Intent clickIntent1 = new Intent(this.getApplicationContext(), WidgetProvider.class);

				clickIntent1.setAction(WidgetProvider.ACTION_UPDATE_CLICK); 
				clickIntent1.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

				PendingIntent pendingIntent1 = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent1,
						PendingIntent.FLAG_UPDATE_CURRENT);
				remoteViews.setOnClickPendingIntent(R.id.imgUpdate, pendingIntent1);

				// Register onClickListener for forward button
				Intent clickIntent2 = new Intent(this.getApplicationContext(), WidgetProvider.class);

				clickIntent2.setAction(WidgetProvider.ACTION_UPDATE_FORWARD); 
				clickIntent2.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

				PendingIntent pendingIntent2 = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent2,
						PendingIntent.FLAG_UPDATE_CURRENT);
				remoteViews.setOnClickPendingIntent(R.id.imgForward, pendingIntent2);

				// Register onClickListener for backward button
				Intent clickIntent3 = new Intent(this.getApplicationContext(), WidgetProvider.class);

				clickIntent3.setAction(WidgetProvider.ACTION_UPDATE_BACKWARD); 
				clickIntent3.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

				PendingIntent pendingIntent3 = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent3,
						PendingIntent.FLAG_UPDATE_CURRENT);
				remoteViews.setOnClickPendingIntent(R.id.imgBackward, pendingIntent3);

				// Register onClickListener for favorite button
				Intent clickIntent5 = new Intent(this.getApplicationContext(), WidgetProvider.class);

				clickIntent5.setAction(WidgetProvider.ACTION_UPDATE_FAVORITE); 
				clickIntent5.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

				PendingIntent pendingIntent5 = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent5,
						PendingIntent.FLAG_UPDATE_CURRENT);
				remoteViews.setOnClickPendingIntent(R.id.imgFavorite, pendingIntent5);

				// Register onClickListener for Text button
				Intent clickIntent6 = new Intent(this.getApplicationContext(), WidgetProvider.class);

				clickIntent6.setAction(WidgetProvider.ACTION_START_APP); 
				clickIntent6.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

				PendingIntent pendingIntent6 = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent6,
						PendingIntent.FLAG_UPDATE_CURRENT);
				remoteViews.setOnClickPendingIntent(R.id.update_info, pendingIntent6);

				// Set initial title
				Log.d(LOG_TAG, "setTextView " + title);
				remoteViews.setTextViewText(R.id.update_info, title);
				
				// Set initial highlight of favorite imageView (white or yellow)
				if (doc.isFavorite(entry)){
					remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star_sel);
				} else {
					remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star);
				}

				appWidgetManager.updateAppWidget(widgetId, remoteViews);
			}
		}
		}

		if (action.equals(SERVICE_ACTION_UPDATE)) {
			RemoteViews remoteViews = new RemoteViews(this.getPackageName(), R.layout.widget_layout);
			ComponentName watchWidget = new ComponentName(this, WidgetProvider.class);				
			
			remoteViews.setImageViewResource(R.id.imgUpdate, R.drawable.ic_action_refresh_sel);
			appWidgetManager.updateAppWidget(watchWidget, remoteViews);

			int maxPictures = doc.getTotal();
			if(maxPictures > 4000){
				maxPictures = 4000;
			}
			if (maxPictures <= 10) {
				maxPictures = 10;
			}

			Random rand = new Random();
			int randPicture = rand.nextInt(maxPictures) + 1;

			SearchImage si = new SearchImage(this, doc);
			si.getNextImage(randPicture);
			remoteViews.setImageViewResource(R.id.imgUpdate, R.drawable.ic_action_refresh);
			appWidgetManager.updateAppWidget(watchWidget, remoteViews);
		}

		if (action.equals(SERVICE_ACTION_FORWARD)) {
			RemoteViews remoteViews  = new RemoteViews(this.getPackageName(), R.layout.widget_layout);
			ComponentName watchWidget = new ComponentName(this, WidgetProvider.class);				

			remoteViews.setImageViewResource(R.id.imgForward, R.drawable.ic_action_fast_forward_sel);
			appWidgetManager.updateAppWidget(watchWidget, remoteViews);
			
			SearchImage si = new SearchImage(this, doc);
			WallpaperEntry entry = si.setNextImage(true);
			doc.storeDocument();
						
			remoteViews.setTextViewText(R.id.update_info, doc.getActiveWallpaper().getTitle());
			if (doc.isFavorite(entry)){
				remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star_sel);
			} else {
				remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star);
			}

			remoteViews.setImageViewResource(R.id.imgForward, R.drawable.ic_action_fast_forward);
			appWidgetManager.updateAppWidget(watchWidget, remoteViews);
		}

		if (action.equals(SERVICE_ACTION_BACKWARD)) {
			RemoteViews remoteViews  = new RemoteViews(this.getPackageName(), R.layout.widget_layout);
			ComponentName watchWidget = new ComponentName(this, WidgetProvider.class);				

			remoteViews.setImageViewResource(R.id.imgBackward, R.drawable.ic_action_rewind_sel);
			appWidgetManager.updateAppWidget(watchWidget, remoteViews);

			SearchImage si = new SearchImage(this, doc);
			WallpaperEntry entry = si.setNextImage(false);
			doc.storeDocument();
			
			remoteViews.setTextViewText(R.id.update_info, doc.getActiveWallpaper().getTitle());
			if (doc.isFavorite(entry)){
				remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star_sel);
			} else {
				remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star);
			}

			remoteViews.setImageViewResource(R.id.imgBackward, R.drawable.ic_action_rewind);
			appWidgetManager.updateAppWidget(watchWidget, remoteViews);			
		}

		if (action.equals(SERVICE_ACTION_FAVORITE)) {
			doc.setFavorite();
			doc.storeDocument();
		
			// Highlight favorite button
			RemoteViews remoteViews;
			ComponentName watchWidget;
			remoteViews = new RemoteViews(this.getPackageName(), R.layout.widget_layout);
			watchWidget = new ComponentName(this, WidgetProvider.class);			
			
			remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star_sel);
			appWidgetManager.updateAppWidget(watchWidget, remoteViews);
		}

		stopSelf();
//		super.onStart(intent, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
