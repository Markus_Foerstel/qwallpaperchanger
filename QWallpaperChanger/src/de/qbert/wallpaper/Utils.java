package de.qbert.wallpaper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class Utils {
	
	private static final String TAG = "Wallpaper_utils";
	
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    
    //decodes image and scales it to reduce memory consumption
    public static Bitmap decodeFile(File f, int maxWidth, int maxHeight){
    	
    	Log.d(TAG, "decodeFile: " + f.getPath());
    	
    	try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);
  
            // Find the correct scale value. It should be the power of 2,
            // such that the resulting image is smaller than both maxWidth and maxHeight
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2 < maxWidth && height_tmp/2 < maxHeight)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }
  
            // Decode image from file producing the scaled Bitmap.
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        	Log.d(TAG, "decodeFile error: " + e.getMessage());
        }
        return null;
    }
}