package de.qbert.wallpaper.alarm;

import java.util.Calendar;

import de.qbert.wallpaper.WallPaperActivity;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryTime.Event;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class WallpaperReceiver extends BroadcastReceiver
{
	private static final String TAG = "Wallpaper Receiver";

	public static final int REQUEST_CODE = 99;

    @Override
    public void onReceive(Context context, Intent intent)
    {
    	Log.d(TAG, "onReceive called: " + intent.getAction());

        if (intent.getAction().equals(WallPaperActivity.INTENT_UPDATE)) {
    		Intent service = new Intent(context, WallpaperReceiverService.class);
    		context.startService(service);    
        }

      if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
    	
  		AlarmManager alarmMgr;
  		PendingIntent alarmIntent;

  		alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
  		Intent intent1 = new Intent(context, WallpaperReceiver.class);
  		intent1.setData(Uri.parse("timer:" + "999"));
  		intent1.setAction(WallPaperActivity.INTENT_UPDATE);
  		alarmIntent = PendingIntent.getBroadcast(context, WallPaperActivity.REQUEST_CODE_ALARM_INTENT, intent1, 0);
  	
  		// Get Time from document
  		DocBase doc = DocBase.getInstance();
  		Event event = doc.getEvent();
  		
  		// Set the alarm to start at passed in time
  		Calendar calendar = Calendar.getInstance();
  		calendar.setTimeInMillis(System.currentTimeMillis());
  		calendar.set(Calendar.HOUR_OF_DAY, event.getHour());
  		calendar.set(Calendar.MINUTE, event.getMinute());
  	
  		// Remove old intent
  		//alarmMgr.cancel(alarmIntent);
  		
  		alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
      }

    }
}
