package de.qbert.wallpaper.alarm;

import java.util.Calendar;
import java.util.Random;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;
import de.qbert.wallpaper.R;
import de.qbert.wallpaper.SearchImage;
import de.qbert.wallpaper.WallPaperActivity;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryTime.Event;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;

public class WallpaperReceiverService extends Service {
     
	private static final int UPDATE_ID = 1970;
	 
	    @Override
	    public IBinder onBind(Intent arg0)
	    {
	        return null;
	    }
	 
	    @Override
	    public void onCreate() 
	    {
	       super.onCreate();
	    }
	 
	    @Override
	    public void onStart(Intent intent, int startId)
	    {
	    	Boolean dayMatched = false;
	    	
	    	DocBase doc = DocBase.getInstance();
	    	Context context =  this.getApplicationContext();

			SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
			Boolean showNotification = SP.getBoolean("show_notification", false);
	    	
	    	// Get current Day of week
	    	Calendar calendar = Calendar.getInstance();
	    	int day = calendar.get(Calendar.DAY_OF_WEEK); 

	    	Event event = doc.getEvent();
	    	switch (day){
	    	case Calendar.MONDAY:
	    		dayMatched = event.isMonday();
	    		break;
	    	case Calendar.TUESDAY:
	    		dayMatched = event.isTuesday();
	    		break;
	    	case Calendar.WEDNESDAY:
	    		dayMatched = event.isWednesday();
	    		break;
	    	case Calendar.THURSDAY:
	    		dayMatched = event.isThursday();
	    		break;
	    	case Calendar.FRIDAY:
	    		dayMatched = event.isFriday();
	    		break;
	    	case Calendar.SATURDAY:
	    		dayMatched = event.isSaturday();
	    		break;
	    	case Calendar.SUNDAY:
	    		dayMatched = event.isSunday();
	    		break;
	    	}

	    	if (dayMatched){
	    		if (doc.getLastError() != null){
	    			Toast.makeText(this, doc.getLastError(), Toast.LENGTH_LONG).show();
	    		} else {

	    			int maxPictures = doc.getTotal();
	    			if(maxPictures > 4000){
	    				maxPictures = 4000;
	    			}

	    			Random rand = new Random();
	    			int randPicture = rand.nextInt(maxPictures) + 1;

	    			SearchImage si = new SearchImage(this, doc);
	    			si.getNextImage(randPicture);

	    			if (showNotification){
//	    				// Notification
	    			   WallpaperEntry wallpaperEntry = doc.getActiveWallpaper();
//	    			   String notifyMessage = wallpaperEntry.getTitle() + "/n"
//	    					   +  wallpaperEntry.getFileName() + "/n"
//	    					   +  wallpaperEntry.getUpdateTime();
//	    				
//				       mManager = (NotificationManager) this.getApplicationContext().getSystemService(context.NOTIFICATION_SERVICE);
//				       Intent intent1 = new Intent(context, WallPaperActivity.class);
//				     
//				       Notification notification = new Notification(R.drawable.ic_launcher, notifyMessage, System.currentTimeMillis());
//				       intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				 
//				       PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, 0, intent1,PendingIntent.FLAG_UPDATE_CURRENT);
//				       notification.flags |= Notification.FLAG_AUTO_CANCEL;
//				       notification.setLatestEventInfo(context, "Wallpaper Changer", notifyMessage, pendingNotificationIntent);
//				 
//				       mManager.notify(0, notification);
	    				NotificationCompat.Builder mBuilder =
	    				        new NotificationCompat.Builder(this)
	    				        .setSmallIcon(R.drawable.ic_launcher)
	    				        .setContentTitle( wallpaperEntry.getTitle())
	    				        .setContentText( wallpaperEntry.getFileName());
	    				// Creates an explicit intent for an Activity in your app
	    				Intent resultIntent = new Intent(this, WallPaperActivity.class);

	    				// The stack builder object will contain an artificial back stack for the
	    				// started Activity.
	    				// This ensures that navigating backward from the Activity leads out of
	    				// your application to the Home screen.
	    				TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	    				// Adds the back stack for the Intent (but not the Intent itself)
	    				stackBuilder.addParentStack(WallPaperActivity.class);
	    				// Adds the Intent that starts the Activity to the top of the stack
	    				stackBuilder.addNextIntent(resultIntent);
	    				PendingIntent resultPendingIntent =
	    				        stackBuilder.getPendingIntent(
	    				            0,
	    				            PendingIntent.FLAG_UPDATE_CURRENT
	    				        );
	    				mBuilder.setContentIntent(resultPendingIntent);
	    				NotificationManager mNotificationManager =
	    				    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    				// mId allows you to update the notification later on.
	    				mNotificationManager.notify(UPDATE_ID, mBuilder.build());
	    			
	    			}
	    		}
	    	}
	    	stopSelf();
	    }
	 
	    @Override
	    public void onDestroy() 
	    {
	        super.onDestroy();
	    }
	 
	}

