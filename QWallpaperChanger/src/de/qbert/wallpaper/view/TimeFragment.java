package de.qbert.wallpaper.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;
import de.qbert.wallpaper.R;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryTime.Event;


/**
 * A placeholder fragment containing a simple view.
 */
public class TimeFragment extends Fragment //implements TextView.OnEditorActionListener
{
	private static final String TAG = "QWallpaper";
	
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "time_section_number";

	//private LinearLayout searchLayout;
	private EditText etSearch;
	
	private TimePicker timePicker;
	private ToggleButton btnMonday;
	private ToggleButton btnTuesday;
	private ToggleButton btnWednesday;
	private ToggleButton btnThursday;
	private ToggleButton btnFriday;
	private ToggleButton btnSaturday;
	private ToggleButton btnSunday;
	
	private Button btnStartTimer;
	private Button btnStopTimer;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static TimeFragment newInstance(int sectionNumber) {
		TimeFragment fragment = new TimeFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public TimeFragment() {
	}

	@Override
	public void onResume() {
		Log.d(TAG, "onResume TimeFragment");
		DocBase doc = DocBase.getInstance();		
		etSearch.setText(doc.getSearchString());
		super.onResume();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_timer, container, false);
		
		etSearch = (EditText) rootView.findViewById(R.id.editSearchText);
		
		timePicker = (TimePicker) rootView.findViewById(R.id.timePicker);
		btnMonday = (ToggleButton) rootView.findViewById(R.id.btnMonday);
		btnTuesday = (ToggleButton) rootView.findViewById(R.id.btnTuesday);
		btnWednesday = (ToggleButton) rootView.findViewById(R.id.btnWednesday);
		btnThursday = (ToggleButton) rootView.findViewById(R.id.btnThursday);
		btnFriday = (ToggleButton) rootView.findViewById(R.id.btnFriday);
		btnSaturday = (ToggleButton) rootView.findViewById(R.id.btnSaturday);
		btnSunday = (ToggleButton) rootView.findViewById(R.id.btnSunday);
		
		btnStartTimer = (Button) rootView.findViewById(R.id.btnStartTimer);
		btnStopTimer = (Button) rootView.findViewById(R.id.btnStopTimer);

		DocBase doc = DocBase.getInstance();
		if (doc.getLastError() == null){

			Event ev = doc.getEvent();
			
			etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		        @Override
		        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        	
		            if ( (actionId == EditorInfo.IME_ACTION_DONE) ||
		                 (actionId == EditorInfo.IME_ACTION_NEXT) ) {
						   
				        String searchString = etSearch.getText().toString();
					
						DocBase doc = DocBase.getInstance();
						doc.setSearchString(searchString);
						Log.d(TAG, "use search string <" + searchString + ">");
						doc.storeDocument();
						
						searchString = doc.getSearchString();
						etSearch.setText(searchString);
						
						Context ctx = getActivity().getApplicationContext();
						
						 InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
						    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

		                return true;
		            }
		            return false;
		        }
		    });
			
			timePicker.setCurrentHour(ev.getHour());
			timePicker.setCurrentMinute(ev.getMinute());

			btnMonday.setChecked(ev.isMonday());
			btnTuesday.setChecked(ev.isTuesday());
			btnWednesday.setChecked(ev.isWednesday());
			btnThursday.setChecked(ev.isThursday());
			btnFriday.setChecked(ev.isFriday());
			btnSaturday.setChecked(ev.isSaturday());
			btnSunday.setChecked(ev.isSunday());
			
			setButtonState();
		}
		
		return rootView;
	}
	
	public void setButtonState(){
		DocBase doc = DocBase.getInstance();
		if (doc.getLastError() == null){
			Event ev = doc.getEvent();
			if (ev.isActive()){
				btnStartTimer.setEnabled(false);
				btnStopTimer.setEnabled(true);
			} else {
				btnStartTimer.setEnabled(true);
				btnStopTimer.setEnabled(false);				
			}
		} else {
			Toast.makeText(this.getActivity().getApplicationContext(), doc.getLastError(), Toast.LENGTH_LONG).show();
		}
	}
	
	public TimePicker getTimePicker(){
		return timePicker;
	}

	public boolean getBtnMonday() {
		return btnMonday.isChecked();
	}

	public void setBtnMonday(boolean btnMonday) {
		this.btnMonday.setChecked(btnMonday);
	}

	public boolean getBtnTuesday() {
		return btnTuesday.isChecked();
	}

	public void setBtnTuesday(boolean btnTuesday) {
		this.btnTuesday.setChecked(btnTuesday);
	}

	public boolean getBtnWednesday() {
		return btnWednesday.isChecked();
	}

	public void setBtnWednesday(boolean btnWednesday) {
		this.btnWednesday.setChecked(btnWednesday);
	}

	public boolean getBtnThursday() {
		return btnThursday.isChecked();
	}

	public void setBtnThursday(boolean btnThursday) {
		this.btnThursday.setChecked(btnThursday);
	}

	public boolean getBtnFriday() {
		return btnFriday.isChecked();
	}

	public void setBtnFriday(boolean btnFriday) {
		this.btnFriday.setChecked(btnFriday);
	}

	public boolean getBtnSaturday() {
		return btnSaturday.isChecked();
	}

	public void setBtnSaturday(boolean btnSaturday) {
		this.btnSaturday.setChecked(btnSaturday);
	}

	public boolean getBtnSunday() {
		return btnSunday.isChecked();
	}

	public void setBtnSunday(boolean btnSunday) {
		this.btnSunday.setChecked(btnSunday);
	}

	public void setTimePicker(TimePicker timePicker) {
		this.timePicker = timePicker;
	}
}
