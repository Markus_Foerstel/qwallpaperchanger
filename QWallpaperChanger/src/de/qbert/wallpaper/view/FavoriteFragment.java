package de.qbert.wallpaper.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import de.qbert.wallpaper.Utils;
import de.qbert.wallpaper.WallPaperActivity;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;
import de.qbert.wallpaper.R;

public class FavoriteFragment extends ListFragment {

	private static final String TAG_FAVORITE_FRAGMENT = "wallpaper_favorite";
	
	private static final String ARG_SECTION_NUMBER = "favorite_section_number";

	private int mPosition = -1;
	
	
	//--------------------------------------------------------------
	//  Constructor
	//--------------------------------------------------------------
	
	public FavoriteFragment() {
		this.mPosition = -1;
	}

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static FavoriteFragment newInstance(int sectionNumber) {
		FavoriteFragment fragment = new FavoriteFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		if (getDoc().getFavoriteWallpaperList().size() == 0){

		} else {
			MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), getDoc().getFavoriteWallpaperList());
	    	setListAdapter(adapter);
		}
	}
	
	  private DocBase getDoc(){
		  return ((WallPaperActivity)getActivity()).getDoc();
	  }
	
	//--------------------------------------------------------------
	//  Custom Adapter
	//--------------------------------------------------------------

	public class MySimpleArrayAdapter extends ArrayAdapter<WallpaperEntry> {
		  private final Context context;
		  private final List<WallpaperEntry> values;

		  public MySimpleArrayAdapter(Context context,  List<WallpaperEntry> values) {
		    super(context, -1, values);
		    this.context = context;
		    this.values = values;
		  }

		  @Override
		  public View getView(int position, View convertView, ViewGroup parent) {
		    LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View rowView = inflater.inflate(R.layout.favorite_row, parent, false);
		    
		    TextView textViewTitle = (TextView) rowView.findViewById(R.id.title);
		    TextView textViewUrl = (TextView) rowView.findViewById(R.id.Url);
		    TextView textViewTime = (TextView) rowView.findViewById(R.id.Time);
		    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		    Button btnContextMenu = (Button) rowView.findViewById(R.id.btnContextMenu);
		    btnContextMenu.setTag(position);
		    
		    textViewTitle.setText(values.get(position).getTitle());
		    textViewUrl.setText(values.get(position).getUrl());
		    textViewTime.setText(getDate(values.get(position).getUpdateTime(), "dd/MM/yyyy hh:mm:ss"));
			
		    new setWallpaperIcon().execute(values.get(position).getFileName(), imageView);
		    
		    // Install onClick listener for the item popup menu
			btnContextMenu.setOnClickListener(new View.OnClickListener() {
		        public void onClick(View view) {
					startPopupMenu((Integer)view.getTag(), view);
		        }
		    });
		    
		    return rowView;
		  }
		} 
	
	private class setWallpaperIcon extends AsyncTask<Object,Void,Bitmap>{

        ImageView wallpaperIcon = null;

        @Override
        protected Bitmap doInBackground(Object...params) {
            try{
            	File f = new File(getDoc().getDocStoreFolder() + "/" + (String) params[0]);
            	Bitmap eventImage = Utils.decodeFile(f, 110, 110);
                wallpaperIcon =  (ImageView) params[1];
                return eventImage;
            }
            catch(Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap eventImage) {
            if(eventImage!=null && wallpaperIcon!=null){
            	wallpaperIcon.setImageBitmap(eventImage);
            }
        }  

	}
	
    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format 
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date. 
         Calendar calendar = Calendar.getInstance();
         calendar.setTimeInMillis(milliSeconds);
         return formatter.format(calendar.getTime());
    }

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
//			  Toast.makeText(context, "Click ListItem Number " + position, Toast.LENGTH_LONG).show();
	  Log.d(TAG_FAVORITE_FRAGMENT,  "clicked on item: " + position);
	  
	  Context context = getActivity().getApplicationContext();
	  
	  Intent iShowWallpaperActivity = new Intent(context, ShowPictureActivity.class);
	  WallpaperEntry entry = getDoc().getFavoriteWallpaperList().get(position);
	  String filename = entry.getFileName();
	  String path = getDoc().getDocStoreFolder() + "/" + filename;
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_PATH, path);
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_TITLE, entry.getTitle());
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_URL, entry.getUrl());
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_TIME, getDate(entry.getUpdateTime(), "dd/MM/yyyy hh:mm:ss"));
	  
	  // Set Options
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_FAVORITE, "false");
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_DELETE, "true");
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_SHARE, "true");
	  iShowWallpaperActivity.putExtra(ShowPictureActivity.KEY_SET, "true");
	  
	  startActivity(iShowWallpaperActivity);
		
	  super.onListItemClick(l, v, position, id);
	}
	
	  public void notifyDataSetChanged(){
		    MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), getDoc().getFavoriteWallpaperList());
		    setListAdapter(adapter);
		  }

	
	//---------------------------------
    // Context Menu for each list item
    //---------------------------------
    
	public void startPopupMenu(int position, View view){
	    PopupMenu popup = new PopupMenu(getActivity().getApplicationContext(), view);
	    
	    mPosition = position;
	    MenuInflater inflater = popup.getMenuInflater();
	    inflater.inflate(R.menu.favorite_item_menu, popup.getMenu());
	    
	    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
	        public boolean onMenuItemClick(MenuItem item) {
	            switch (item.getItemId()) {  
		        case R.id.item_context_delete:
					((WallPaperActivity)getActivity()).notifyChange(WallPaperActivity.NOTIFICATION_FAVORITE_DELETE, mPosition);
		        	break;
		        		        	
		        case R.id.item_context_set:
					((WallPaperActivity)getActivity()).notifyChange(WallPaperActivity.NOTIFICATION_FAVORITE_SET_WALLPAPER, mPosition);
		        	break;

		        case R.id.item_context_share:
					((WallPaperActivity)getActivity()).notifyChange(WallPaperActivity.NOTIFICATION_FAVORITE_SHARE, mPosition);
		        	break;
		        	
		        default:
		        	// Nothing to do
		            break; 

	            }
	            mPosition = -1;
	            return true;
	        }
	    });
	    
	    popup.show();	
	}

	@Override
	public void onResume() {
		notifyDataSetChanged();
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_view, container, false);
	}

}
