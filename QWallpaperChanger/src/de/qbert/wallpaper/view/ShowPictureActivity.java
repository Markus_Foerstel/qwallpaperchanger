package de.qbert.wallpaper.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import de.qbert.wallpaper.R;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;

public class ShowPictureActivity extends Activity{

	private static final String TAG="Wallpaper";

	public static final String KEY_PATH = "de.qbert.wallpaper.path";
	public static final String KEY_TITLE = "de.qbert.wallpaper.title";
	public static final String KEY_URL = "de.qbert.wallpaper.url";
	public static final String KEY_TIME = "de.qbert.wallpaper.time";

	public static final String KEY_DELETE = "de.qbert.wallpaper.delete";
	public static final String KEY_SET = "de.qbert.wallpaper.set";
	public static final String KEY_FAVORITE = "de.qbert.wallpaper.favorite";
	public static final String KEY_SHARE = "de.qbert.wallpaper.share";

	public static final String KEY_PATH_NOT_SET = "not set";

	private Button btnShowPopUp;

	private ImageView imgShowWallpaper;
	private TextView txtShowTitle;
	private TextView txtShowPath;
	private TextView txtShowTime;

	private boolean showDelete = false;
	private boolean showSet = false;
	private boolean showFavorite = false;
	private boolean showShare = false;

	private String path = KEY_PATH_NOT_SET;
	private String title = KEY_PATH_NOT_SET;
	private String url = KEY_PATH_NOT_SET;
	private String time = KEY_PATH_NOT_SET;
	
	private Context context = null;
	
	private Matrix matrixScale = new Matrix();
	private Matrix matrixTrans = new Matrix();
	private Matrix matrix = new Matrix();
	private float scale = 1.0f;
	private float xPos = 0.0f;
	private float yPos = 0.0f;
	private ScaleGestureDetector mScaleGestureDetector;
    private GestureDetectorCompat mGestureDetector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_wallpaper);
		
		context = this;
		
		imgShowWallpaper = (ImageView)findViewById(R.id.imgShowWallpaper);
		
		/* Set up gesture detection */
		mScaleGestureDetector =  new ScaleGestureDetector(this, mScaleGestureListener);
		mGestureDetector = new GestureDetectorCompat(context, mGestureListener);

		Intent myIntent = getIntent();
		path = myIntent.getStringExtra(KEY_PATH);
		title = myIntent.getStringExtra(KEY_TITLE);
		url = myIntent.getStringExtra(KEY_URL);
		time = myIntent.getStringExtra(KEY_TIME);

		String strShowDelete = myIntent.getStringExtra(KEY_DELETE);
		showDelete = ( (strShowDelete != null) && (strShowDelete.equalsIgnoreCase("true")));

		String strShowSet = myIntent.getStringExtra(KEY_SET);
		showSet = ( (strShowSet != null) && (strShowSet.equalsIgnoreCase("true")));

		String strShowFavorite = myIntent.getStringExtra(KEY_FAVORITE);
		showFavorite = ( (strShowFavorite != null) && (strShowFavorite.equalsIgnoreCase("true")));

		String strShowShare = myIntent.getStringExtra(KEY_SHARE);
		showShare = ( (strShowShare != null) && (strShowShare.equalsIgnoreCase("true")));

		btnShowPopUp = (Button) findViewById(R.id.btnShowPopUp);

		txtShowTitle = (TextView) findViewById(R.id.txtShowTitle);
		txtShowPath = (TextView) findViewById(R.id.txtShowPath);
		txtShowTime = (TextView) findViewById(R.id.txtShowTime);

		txtShowTitle.setText(title);
		txtShowPath.setText(path);
		txtShowTime.setText(time);
		
		btnShowPopUp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startPopupMenu(v);
			}
			});
	}


	@Override
	public void onWindowFocusChanged (boolean hasFocus) {	
		if (!path.equalsIgnoreCase(KEY_PATH_NOT_SET)){
			new setWallpaperIcon().execute(path, imgShowWallpaper, imgShowWallpaper.getWidth(), imgShowWallpaper.getHeight());
		}
	}

	private class setWallpaperIcon extends AsyncTask<Object,Void,Bitmap>{

		ImageView wallpaperIcon = null;

		@Override
		protected Bitmap doInBackground(Object...params) {
			try{
				Log.d(TAG,  "process image: " + (String)params[0] + "w:" + (Integer) params[2] + "h:" + (Integer) params[3]);
				Bitmap eventImage = decodeFile((String) params[0], (Integer) params[2], (Integer) params[3]);
				wallpaperIcon =  (ImageView) params[1];
				return eventImage;
			}
			catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(Bitmap eventImage) {
			if(eventImage!=null && wallpaperIcon!=null){
				wallpaperIcon.setImageBitmap(eventImage);
			}
		}  
	}

	public void setWallpaper(){
		DisplayMetrics metrics = new DisplayMetrics(); 
		WindowManager winMgr = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);

		winMgr.getDefaultDisplay().getMetrics(metrics);
		// get the height and width of screen 
		int height = metrics.heightPixels; 
		int width = metrics.widthPixels;

		// get the Image as Bitmap 
		Bitmap bitmap = decodeFile(path, width, height);

		WallpaperManager wallpaperManager = WallpaperManager.getInstance(this); 
		try {
			wallpaperManager.suggestDesiredDimensions(width, height);
			wallpaperManager.setBitmap(bitmap);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(String path, int width, int height){
		try {
			File f = new File(path);
			//decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);

			//Find the correct scale value. It should be the power of 2.
			final int REQUIRED_WIDTH=width;
			final int REQUIRED_HEIGHT=height;
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			while(true){
				if(width_tmp/2<REQUIRED_WIDTH || height_tmp/2<REQUIRED_HEIGHT)
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale*=2;
			}

			//decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {}
		return null;
	}
	
	
    //---------------------------------
    // Context Menu for each list item
    //---------------------------------
    
	public void startPopupMenu(View view){
	    PopupMenu popup = new PopupMenu(this, view);
	    
	    MenuInflater inflater = popup.getMenuInflater();
	    inflater.inflate(R.menu.recent_item_menu, popup.getMenu());
	    	    
	    popup.getMenu().getItem(0).setEnabled(showDelete);
	    popup.getMenu().getItem(1).setEnabled(showSet);
	    popup.getMenu().getItem(2).setEnabled(showFavorite);
	    popup.getMenu().getItem(3).setEnabled(showShare);
	    
	    MenuItem item = popup.getMenu().findItem(R.id.item_context_favorite);
	    item.setVisible(false);
	    
	    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
	        public boolean onMenuItemClick(MenuItem item) {
	        	DocBase doc;
	        	
	            switch (item.getItemId()) {  
		        case R.id.item_context_delete:
					Log.d("TAG", "onClick on Delete");
					doc = DocBase.getInstance();
					if (doc.getLastError() == null) {
						// Update document
						if (path.indexOf("/Recent/") != -1){
							doc.deleteRecentWallpaper(url);
						} else {
							doc.deleteFavoriteWallpaper(url);
						}
						// Make change persistent
						doc.storeDocument();
				        Toast.makeText(context, getString(R.string.show_del), Toast.LENGTH_LONG).show();
						finish();
					}
		        	break;

		        case R.id.item_context_set:
					setWallpaper();
			        Toast.makeText(context, getString(R.string.show_set), Toast.LENGTH_LONG).show();
		        	break;

		        case R.id.item_context_favorite:
					Log.d("TAG", "onClick on Favorite");
					doc = DocBase.getInstance();
					if (doc.getLastError() == null){				
						WallpaperEntry entry = doc.searchRecentEntryByUrl(url);
						if (entry != null) {
							doc.setFavorite(entry);
							doc.storeDocument();
					        Toast.makeText(context, getString(R.string.show_fav), Toast.LENGTH_LONG).show();
						}
					}

		        	break;

		        case R.id.item_context_share:
					Log.d("TAG", "onClick on Share");
					Intent sharingIntent = new Intent(Intent.ACTION_SEND);
					sharingIntent.setType("image/jpeg");
					String locPath = "file://" + path;
					sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(locPath));
					sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, url);
					startActivity(Intent.createChooser(sharingIntent, getString(R.string.infoSharedVia)));
		        	break;
		        	
		        default:
		        	// Nothing to do
		            break; 

	            }
	            return true;
	        }
	    });
	    
	    popup.show();	
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
	    boolean retVal = mScaleGestureDetector.onTouchEvent(event);
	    retVal = mGestureDetector.onTouchEvent(event) || retVal;
	    return retVal || super.onTouchEvent(event);
	}


	@Override
	protected void onResume() {
//		DisplayMetrics metrics = new DisplayMetrics();
//		getWindowManager().getDefaultDisplay().getMetrics(metrics);
//		int width = metrics.widthPixels;
//		int height = metrics.heightPixels;
		super.onResume();
	}

	
	private void setImageViewTransform(){
		matrixScale.setScale(scale, scale);
		matrixTrans.setTranslate(xPos, yPos);
		matrix.setConcat(matrixScale, matrixTrans);
		imgShowWallpaper.setImageMatrix(matrix);
		Log.d(TAG,  "scale: " + scale);		
	}
	
	
	/**
	 * The gesture listener, used for handling simple gestures such as double touches, scrolls,
	 * and flings.
	 */
	private final GestureDetector.SimpleOnGestureListener mGestureListener 
	= new GestureDetector.SimpleOnGestureListener() {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			xPos -= distanceX * 0.5f;
			yPos -= distanceY * 0.5f;				
			setImageViewTransform();
			return true;
		}
	};

	
    /**
     * The scale listener, used for handling multi-finger scale gestures.
     */
    private final ScaleGestureDetector.OnScaleGestureListener mScaleGestureListener
            = new ScaleGestureDetector.SimpleOnScaleGestureListener() {

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
			scale *= scaleGestureDetector.getScaleFactor();
			scale = Math.max(0.1f, Math.min(scale, 5.0f));  
            setImageViewTransform();
            return true;
        }
    };
    	
}

