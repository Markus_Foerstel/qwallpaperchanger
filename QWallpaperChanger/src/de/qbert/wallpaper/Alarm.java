package de.qbert.wallpaper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.ImageColumns;
import android.util.Log;
import android.widget.Toast;

public class Alarm extends BroadcastReceiver 
{    

	private static final String TAG="Alarm";
	
	private Random mRand;
	
	@Override
    public void onReceive(Context context, Intent intent) 
    {   
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        try {
            wl.acquire();

            mRand = new Random();
    
            WallpaperManager myWallpaperManager = WallpaperManager.getInstance(context);

    	    Log.d(TAG, "we start here");

        	Bitmap bitmap = setRandomImage(context);
            myWallpaperManager.setBitmap(bitmap);
    
        } catch (IOException e) {
        	e.printStackTrace();
        } finally {
            wl.release();
        }
    }
    
    public Bitmap setRandomImage(Context context){
		ContentResolver cr = context.getContentResolver();

		String[] columns = new String[] {
		                ImageColumns._ID,
		                ImageColumns.TITLE,
		                ImageColumns.DATA,
		                ImageColumns.MIME_TYPE,
		                ImageColumns.SIZE };
		
		Cursor cur = null;
		try{
			cur = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
		                columns, null, null, null);

		}catch (Exception e){
			Log.v(this.getClass().getName(), e.getMessage());
		}

		// if there are no images we give up
		if (cur == null){
			Toast.makeText(context, context.getString(R.string.message_no_pics), 
					Toast.LENGTH_SHORT).show();
			return null;
		}
		
		int numImages = cur.getCount();
		
		if (numImages == 0){
			Toast.makeText(context, context.getString(R.string.message_no_pics), 
						Toast.LENGTH_SHORT).show();
			return null;
		}
		
		int selImage = mRand.nextInt(numImages);
		
		if (!cur.move(selImage + 1)){
			Toast.makeText(context, context.getString(R.string.message_no_pics), 
					Toast.LENGTH_SHORT).show();
			return null;
		}
		
		int    imageID = cur.getInt(0);
		
		Uri uri = Uri.withAppendedPath( MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                Integer.toString(imageID) );

		cur.close();

        Toast.makeText(context, context.getString(R.string.message_loading_pic) + uri.toString(), Toast.LENGTH_LONG).show();

		// Load the bitmap
		return getBitmap(context, uri);
    }
    
	private Bitmap getBitmap(Context context, Uri uri) {

    	InputStream in = null;
    	try {
    		ContentResolver contentResolver = context.getContentResolver();
    		
    	    final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
    	    in = contentResolver.openInputStream(uri);

    	    // Decode image size
    	    BitmapFactory.Options o = new BitmapFactory.Options();
    	    o.inJustDecodeBounds = true;
    	    BitmapFactory.decodeStream(in, null, o);
    	    in.close();

    	    int scale = 1;
    	    while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > 
    	          IMAGE_MAX_SIZE) {
    	       scale++;
    	    }
    	    Log.d(TAG, "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

    	    Bitmap b = null;
    	    in = contentResolver.openInputStream(uri);
    	    if (scale > 1) {
    	        scale--;
    	        // scale to max possible inSampleSize that still yields an image
    	        // larger than target
    	        o = new BitmapFactory.Options();
    	        o.inSampleSize = scale;
    	        b = BitmapFactory.decodeStream(in, null, o);

    	        // resize to desired dimensions
    	        int height = b.getHeight();
    	        int width = b.getWidth();
    	        Log.d(TAG, "1th scale operation dimenions - width: " + width + ", height: " + height);

    	        double y = Math.sqrt(IMAGE_MAX_SIZE
    	                / (((double) width) / height));
    	        double x = (y / height) * width;

    	        Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, 
    	           (int) y, true);
    	        b.recycle();
    	        b = scaledBitmap;

    	        System.gc();
    	    } else {
    	        b = BitmapFactory.decodeStream(in);
    	    }
    	    in.close();

    	    Log.d(TAG, "bitmap size - width: " +b.getWidth() + ", height: " + 
    	       b.getHeight());
    	    return b;
    	} catch (IOException e) {
    	    Log.e(TAG, e.getMessage(),e);
    	    return null;
    	}
    }


    public void SetAlarm(Context context)
    {
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 10, pi); // Millisec * Second * Minute
    }

    public void CancelAlarm(Context context)
    {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}