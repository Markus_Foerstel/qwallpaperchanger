package de.qbert.wallpaper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import javax.net.ssl.HttpsURLConnection;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RemoteViews;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;

public class SearchImage extends SearchImageBase {

	private static final String  API_KEY = "15098300-d26be84ef79146ff6af75b791";
	
	// The pixabi Rest interface can be configured to return max 200 entries
	private static final int  MAX_RESULTS_PER_PAGE = 200;

	private static final String  NO_WALLPAPER_FOUND = "-- nothing found ---";
	
	private static final int  SERVER_RETRIES = 3;
	
	// Space left to the right and left of the progress bar
	private static final int WIDGET_PROGRESS_MARGIN = 7;
	
	private static final int PROGRESS_BAR_RED = -1;
	private static final int PROGRESS_BAR_GREEN = -2;

	private static final int CONNECTION_TIMEOUT = 10000;
	private static final int READ_TIMEOUT = 10000;
	
	private Context context;
	
	private boolean running;
	
	private SearchFilesTask searchFileTask;
	
	public SearchImage(Context context, DocBase doc) {
		super(context, doc);
		this.context = context;
	}

	public void getNextImage(int number) {
		running = true;
		searchFileTask = new SearchFilesTask();
		searchFileTask.execute(number , null, null);
		try {
			searchFileTask.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	class SearchFilesTask extends AsyncTask<Integer, Integer, String> {

		String url;
		String title;
		int total;
		int progress;

		@Override
		protected void onPreExecute() {
			url = null;
			title = null;
			total = 0;
			progress = 10;
			super.onPreExecute();
		}
		
		protected String doInBackground(Integer... randNumber) {

			Random rand = new Random();				
			int myRandom = randNumber[0];
			
			int retries = SERVER_RETRIES;
		
			progress= 20;
			publishProgress(progress);
			
			cleanUp();
			publishProgress(progress += 20);
			
			do {					
				// Request new URL from server
				String response = requestPictureUrlFromServer();
				if ( (response != null) ){//&& (getResponseStatus(response)) ){

					total = getTotal(response);
					if (total != 0) {
						doc.setTotal(total);

						int maxTotal = Math.min(total, MAX_RESULTS_PER_PAGE);

						myRandom = rand.nextInt(maxTotal);
					}
					
					url = getAddress(response, myRandom);
					
					title = getTitle(response, myRandom);
					//title = "no title";

					if ((total != 0) && (url != null) && (title != null) ) {

						this.publishProgress(progress += 20);

						// Download image
						if (getBitmap(url) != null) {

							publishProgress(progress += 20);
	
							// Create new entry for wallpaper in document
							doc.addWallpaper(url, title, doc.getSearchString()); //getSearchString());
	
							publishProgress(progress += 20);
							
							String path = doc.getDocRecentFolder() + "/" + doc.getName(url);
							setWallpaper(path);

							retries = 0;
						
						} else {
							title = null;
							int print_retiries = SERVER_RETRIES -retries + 1;
							Log.d(TAG_ERR, "ERROR: Cannot store wallpaper on file system. Attempt: " + print_retiries);							
						}
					} else {
						int print_retiries = SERVER_RETRIES -retries + 1;
						Log.d(TAG_ERR, "ERROR: Server response was not ok. Attempt: " + print_retiries);
					}
				} else {
					int print_retiries = SERVER_RETRIES -retries + 1;
					Log.d(TAG_ERR, "ERROR: Server response was not ok. Attempt: " + print_retiries);
				}
				retries--;
				
				progress += 10;
				publishProgress(progress);
				
			} while ( (title == null) && (retries >= 0) );
			
			if (title == null) {
				publishProgress(PROGRESS_BAR_RED);
			} else {
				publishProgress(PROGRESS_BAR_GREEN);
			}
						
			return title;
		}

		protected void onProgressUpdate(Integer... progress) {
			updateWidgetProgressBitmap(progress[0]);
		}

		protected void onPostExecute(String result) {
			if (result != null) {
				
				// And make persistent
				Log.d(TAG, "ACTION_UPDATE_CLICK store document... ");
				doc.storeDocument();
				Log.d(TAG, "ACTION_UPDATE_CLICK store document DONE");
				
				Log.d(TAG, "Notify ....");
				if (WallPaperActivity.class.equals(context.getClass())){
					WallPaperActivity wapplaperContext = (WallPaperActivity)context;
					wapplaperContext.notifyChange(WallPaperActivity.NOTIFICATION_NEW_RECENT, WallPaperActivity.NOTIFICATION_NO_DATA);
					updateWidgetTitle(result);			
					Log.d(TAG, "... Main Controller");
				} else {
					updateWidgetTitle(result);			
					Log.d(TAG, "... no one");
				}
			} else {
				if (WallPaperActivity.class.equals(context.getClass())){
					WallPaperActivity wapplaperContext = (WallPaperActivity)context;
					wapplaperContext.notifyChange(WallPaperActivity.NOTIFICATION_NO_WALLPAPER_FOUND, WallPaperActivity.NOTIFICATION_NO_DATA);
					updateWidgetTitle(NO_WALLPAPER_FOUND);
					Log.d(TAG, "... Main Controller");
				} else {
					updateWidgetTitle(NO_WALLPAPER_FOUND);
					Log.d(TAG, "... no one");
				}
			}
			
			running = false;
		}

		@Override
		protected void onCancelled(String result) {
			running = false;
			super.onCancelled(result);
		}

		@Override
		protected void onCancelled() {
			running = false;
			super.onCancelled();
		}
	}
	
	public boolean isRunning(){
		return running;
	}
	
	private void updateWidgetTitle(String title){

		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

		ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
		
		for (int widgetId : allWidgetIds) {
			RemoteViews remoteViews;
			remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
			remoteViews.setTextViewText(R.id.update_info, title);
			// TODO: The new wallpaper could be already a favorite
			remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star);
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
	}
	
	private void updateWidgetProgressBitmap(int percent) {
		
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        int cwidth = metrics.widthPixels;
//        int cheight = metrics.heightPixels;
        int cheight = 4;
        int alpha;
		
        Paint redPaint = new Paint();
        redPaint.setARGB(88, 255, 55, 55);
        Paint greenPaint = new Paint();
        greenPaint.setARGB(88, 55, 255, 55);
        
//        Log.d(TAG, "2 - create Paint done w:" + cwidth + " ,h:" + cheight);

        Bitmap bitmap = Bitmap.createBitmap(cwidth, cheight, Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//
//        Log.d(TAG, "3 - create Canvas done");
//        
//        //canvas.drawColor(Color.MAGENTA);
//        
//        //canvas.drawPaint(redPaint);
//        //redPaint.setStyle(Paint.Style.FILL);
//        redPaint.setStrokeWidth(1);
//        
////      if ((percent == PROGRESS_BAR_RED) || (percent == PROGRESS_BAR_GREEN)) {
//    	  canvas.drawLine(0.0f, 0.0f, 100.0f, 1.0f, redPaint);
////      } else {
////   
        
		int color = Color.argb(88, 55, 255, 55);

		if (percent == PROGRESS_BAR_RED) {
			color = Color.argb(88, 255, 55, 55);
		}

		if ((percent == PROGRESS_BAR_RED) || (percent == PROGRESS_BAR_GREEN))
			for (int y = 0; y < cheight; y++) {
				for (int x = 0; x < cwidth; x++) {
					bitmap.setPixel(x, y, color);
				}
			}
		else {
			int fillWidth = (int) ((float) cwidth * (float) percent / 100.0f);

			for (int y = 0; y < cheight; y++) {
				for (int x = 0; x < cwidth; x++) {
					if ((x < WIDGET_PROGRESS_MARGIN)
							|| (x > cwidth - WIDGET_PROGRESS_MARGIN)) {
						alpha = 0;
					} else {
						alpha = 0x88;
					}

					if (x < fillWidth) {
						bitmap.setPixel(x, y, Color.argb(alpha, 55, 55, 255));
					} else {
						bitmap.setPixel(x, y, Color.argb(0, 55, 55, 255));
					}
				}
			}
		}
 		
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		RemoteViews remoteViews;
		ComponentName watchWidget;
		remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		watchWidget = new ComponentName(context, WidgetProvider.class);

	    remoteViews.setImageViewBitmap(R.id.imgProgress, bitmap);

		appWidgetManager.updateAppWidget(watchWidget, remoteViews);	
	}

	private String getSearchString(){
		String searchString = doc.getSearchString();
		if (searchString.isEmpty()) {
			searchString = "wallpaper";
		} else {
			searchString = searchString.replace(" ", "+");
		}
		
		Log.d(TAG, "use search string <" + searchString + ">");
		 
		return searchString;		
	}
	
	private String requestPictureUrlFromServer(){
		String response = null;
		
		String searchString = getSearchString();

		//HttpClient httpClient = new DefaultHttpClient();
		
		// Example: URL request
		// https://pixabay.com/api/?key=15098300-d26be84ef79146ff6af75b791&q=yellow+flowers&image_type=photo
			
		String requestString = new String("https://pixabay.com/api/?" 
				+ "key=" + API_KEY
				+ "&per_page=" + MAX_RESULTS_PER_PAGE
				+ "&q=" + searchString									// e.g. wallpaper+scifi"
				+ "&image_type=photo");

		Log.d(TAG, requestString);
		
		URL myUrl;
		try {
			myUrl = new URL(requestString);
	        HttpsURLConnection conn = (HttpsURLConnection)myUrl.openConnection();
	        InputStream is = conn.getInputStream();
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);
	
	        String inputLine;
	        
			StringBuilder builder = new StringBuilder();
	
	        while ((inputLine = br.readLine()) != null) {
	    		builder.append(inputLine);
	            System.out.println(inputLine);
	        }
	
			response = new String(builder);

			Log.d(TAG, response);
	
	        br.close();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			response = null;
		}
		catch (IOException e) {
			e.printStackTrace();
			response = null;
		}
		
		return response;
	}
	
	//	<rsp stat="ok">
	//	<photos page="3407" pages="5579" perpage="1" total="5579">
	//		<photo id="6804151471" owner="29980644@N07" secret="306077bf22" server="7160" farm="8" title="Green_1680" ispublic="1" isfriend="0" isfamily="0" />
	//	</photos>
	//	</rsp>
	private boolean getResponseStatus(String response){
		return (response.indexOf("stat=\"ok\"") != -1);
	}

	private Bitmap getBitmap(String url)
	{
		File f = new File(doc.getDocRecentFolder(), doc.getName(url));

		try {
			Bitmap bitmap=null;
			URL imageUrl = new URL(url);
			//URL imageUrl = new URL("https://pixabay.com/get/55e0d340485aa814f6da8c7dda79367b1536d6e653516c48702773d2904bc259be_1280.jpg");
			HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
			conn.setConnectTimeout(CONNECTION_TIMEOUT);
			conn.setReadTimeout(READ_TIMEOUT);
			conn.setInstanceFollowRedirects(true);
			InputStream is=conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			bitmap = decodeFile(f);
			return bitmap;
		} catch (Exception ex){
			ex.printStackTrace();
			return null;
		}
	}

	//decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f){
		BitmapFactory.Options o = new BitmapFactory.Options();
		try {
			//decode image size
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		//Find the correct scale value. It should be the power of 2.
		final int REQUIRED_SIZE=960;
		int width = o.outWidth;
		int height = o.outHeight;
		int width_tmp = o.outWidth; //, height_tmp = o.outHeight;
		int scale=1;
		while(true){
			if(width_tmp/2<REQUIRED_SIZE ) //|| height_tmp/2<REQUIRED_SIZE)
				break;
			width_tmp/=2;
			//height_tmp/=2;
			scale*=2;
		}

		try {
			//decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			
			Log.d(TAG, "Scale: " + scale);
			Log.d(TAG, "Width: " + width);
			Log.d(TAG, "Height: " + height);

			Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public int getTotal(String json){
		int start;
		int end;

		start = json.indexOf("total\"");
		start = json.indexOf(":", start);
		end = json.indexOf(",", start+1);
		return Integer.valueOf(json.substring(start+1, end));
	}

	/**
	 *
	 * @param jason
	 */
	public String getTitle(String jason, int index){
		String title;
		int start=0;
		int end=0;

		// "pageURL":"https://pixabay.com/photos/steampunk-city-clock-clock-city-3006650/"
		for (int i = 0; i< index; i++) {
			start = jason.indexOf("pageURL\":", start);
			if (start == -1)
				return null;
			start = jason.indexOf("h", start);
			end = jason.indexOf("\"", start+1);
		}

		String path = jason.substring(start, end);
		String parts[] = path.split("/");
		title = parts[parts.length-1];
		Log.d(TAG, "Title: " + title);
		
		return title;
	}

	/**
	 *
	 * @param Pixabi response
	 */
	public String getAddress(String jason, int index){
		String address;
		int start=0;
		int end=0;

		//largeImageURL":
		// "webformatURL":"https://pixabay.com/get/55e0d5454c57ac14f1dc84609629327e1c37dde5534c704c7d277ddc974bcd5c_640.jpg"
		for (int i = 0; i< index; i++) {
			start = jason.indexOf("largeImageURL\":", start);
			if (start == -1)
				return null;
			start = jason.indexOf("h", start);
			end = jason.indexOf("\"", start+1);
		}

		address = jason.substring(start, end);
		Log.d(TAG, "Address: " + address);
		
		return address;
	}

	// <img class="mainImage" src="http://www.berrytheme.com/wallpapers/5000-6000/5367/file/5367.jpg" data-bm="40">
	public String findImgSrc(String input){

		final String IMG_TAG="<img ";

		String res = null;

		int indexStart = 0;
		int indexEnd = 0;

		do {
			indexStart = input.indexOf(IMG_TAG, indexStart);
			if (indexStart != -1) {
				indexStart += IMG_TAG.length();
				indexEnd = input.indexOf(">", indexStart);

				res = input.substring(indexStart, indexEnd);
				Log.d(TAG, res);
			}
		}
		while (indexStart != -1);

		return res;
	}
	
	private void cleanUp(){
		// Get list of files in document
		List<WallpaperEntry> lstFilesTempDoc = doc.getRecentWallpaperList();
		
		List<String> lstFilesDoc = new ArrayList<String>();
		for (WallpaperEntry entry: lstFilesTempDoc){
			lstFilesDoc.add(entry.getFileName());
		}

		// Get list of actual files in Wallpaper folder
		File files[];
		List<String> lstFiles = new ArrayList<String>();

		String path = doc.getDocRecentFolder();
		Log.d(TAG, "Path: " + path);
		File f = new File(path);        
		files = f.listFiles();
		Log.d(TAG, "Size: "+ files.length);
		for (int i=0; i < files.length; i++)
		{
			Log.d("Files", "FileName:" + files[i].getName());
			lstFiles.add(files[i].getName());
		}

		// Find files in folder that are not referenced in document.
		lstFiles.removeAll( lstFilesDoc );
		
		// Delete unreferenced files
		for (String entry: lstFiles){
			String strDelFile =  doc.getDocRecentFolder() + "/" + entry;
			File delFile = new File(strDelFile);
			if (!delFile.delete()){
				Log.d(TAG, "File deletion failed on file: "+ strDelFile);
			} else {
				Log.d(TAG, "File deleted: "+ strDelFile);					
			}
		}		
	}
}

