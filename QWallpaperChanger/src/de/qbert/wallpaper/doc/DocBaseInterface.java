package de.qbert.wallpaper.doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public interface DocBaseInterface {

	public boolean storeElement(BufferedWriter bw) throws IOException;

	public boolean loadElement(BufferedReader br) throws IOException;
	
}
