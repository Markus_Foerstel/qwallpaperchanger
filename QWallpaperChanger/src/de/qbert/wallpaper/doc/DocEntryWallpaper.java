package de.qbert.wallpaper.doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import android.util.Log;

public class DocEntryWallpaper extends DocBaseEntry implements DocBaseInterface {

	private static final String TAG = "Wallpaper";
	
	private static String INDENT = "   ";

	public static final String TAG_WALLPAPER_LIST = "<WallpaperList>";
	public static final String TAG_WALLPAPER_LIST_CLOSE = "</WallpaperList>";
	private static final String TAG_WALLPAPER = "<Wallpaper>";
	private static final String TAG_URL = "<url>";
	private static final String TAG_TITLE = "<title>";
	private static final String TAG_UPDATETIME = "<updateTime>";
	private static final String TAG_ACTIVE = "<active>";
	private static final String TAG_FAVORITE = "<favorite>";
	private static final String TAG_SEARCHTAGS = "<searchtags>";
	private static final String TAG_WALLPAPER_CLOSE = "</Wallpaper>";

	private static final String DEFAULT_NOT_SET = "not set";

	public class WallpaperEntry{
		private String url;
		private String title;
		private Boolean active;
		private long updateTime;  // Time the wallpaper was added to the document
		private String searchtags;

		public WallpaperEntry(String url, String title, long updateTime, Boolean active, String searchtags){
			this.url = url;
			this.title = title;
			this.updateTime = updateTime;
			this.active = active;
			this.setSearchtags(searchtags);
		}

		public String getUrl() {
			return url;
		}

		public String getFileName(){
			int pos = url.lastIndexOf("/");
			return url.substring(pos+1);
		}

		public String getTitle(){
			return title;
		}

		public long getUpdateTime() {
			return updateTime;
		}

		public Boolean getActive() {
			return active;
		}

		public void setActive(Boolean active) {
			this.active = active;
		}

		/**
		 * @return the searchtags
		 */
		public String getSearchtags() {
			return searchtags;
		}

		/**
		 * @param searchtags the searchtags to set
		 */
		public void setSearchtags(String searchtags) {
			this.searchtags = searchtags;
		}
	}

	List<WallpaperEntry> lstRecentWallpaper;
	List<WallpaperEntry> lstFavoriteWallpaper;

	private DocBase parent = null;

	DocEntryWallpaper(DocBase parent) {
		super();
		this.parent = parent;
		lstRecentWallpaper = new ArrayList<WallpaperEntry>();
		lstFavoriteWallpaper = new ArrayList<WallpaperEntry>();
	}

	public void addWallpaperEntry(String url, String title, String searchtags){

		// Only add entry if new
		for(Iterator<WallpaperEntry> it = lstRecentWallpaper.iterator(); it.hasNext();){
			if (it.next().getUrl().equalsIgnoreCase(url))
				return;
		}

		// Reset activation as we know we will add a new active wallpaper
		for(Iterator<WallpaperEntry> it = lstRecentWallpaper.iterator(); it.hasNext();){
			it.next().setActive(false);
		}

		int numMaxEntries = getMaxRecentEntries();

		if (lstRecentWallpaper.size() < numMaxEntries){
			lstRecentWallpaper.add( new WallpaperEntry(url, title, System.currentTimeMillis(), true, searchtags));
		} else {
			// Delete oldest entry ...		
			lstRecentWallpaper.remove(lstRecentWallpaper.size()-1);
			// and add a new one at the beginning of the list
			lstRecentWallpaper.add( 0, new WallpaperEntry(url, title, System.currentTimeMillis(), true, searchtags));
		}
	}

	@Override
	public boolean storeElement(BufferedWriter bw) throws IOException {

		StringBuilder builder = new StringBuilder();
		builder.append(TAG_WALLPAPER_LIST + System.getProperty("line.separator"));

		for(Iterator<WallpaperEntry> it = lstRecentWallpaper.iterator(); it.hasNext();){
			WallpaperEntry entry = it.next();

			builder.append(INDENT + TAG_WALLPAPER + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_URL + entry.url + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_TITLE + entry.title + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_UPDATETIME + entry.updateTime + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_ACTIVE + entry.active + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_FAVORITE + Boolean.FALSE.toString() + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_SEARCHTAGS + entry.getSearchtags() + "</>"  + System.getProperty("line.separator"));
			
			builder.append(TAG_WALLPAPER_CLOSE  + System.getProperty("line.separator"));
		}

		for(Iterator<WallpaperEntry> it = lstFavoriteWallpaper.iterator(); it.hasNext();){
			WallpaperEntry entry = it.next();

			builder.append(INDENT + TAG_WALLPAPER + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_URL + entry.url + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_TITLE + entry.title + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_UPDATETIME + entry.updateTime + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_ACTIVE + entry.active + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_FAVORITE + Boolean.TRUE.toString() + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_SEARCHTAGS + entry.getSearchtags() + "</>"  + System.getProperty("line.separator"));
			
			builder.append(TAG_WALLPAPER_CLOSE  + System.getProperty("line.separator"));
		}

		builder.append(TAG_WALLPAPER_LIST_CLOSE + System.getProperty("line.separator"));
		bw.write(new String(builder));

		return true;
	}

	@Override
	public boolean loadElement(BufferedReader br) throws IOException {

		boolean cont = true;
		String line;
		String element;
		String title = DEFAULT_NOT_SET;
		String url = DEFAULT_NOT_SET;
		boolean active = false;
		boolean favorite = false;
		boolean activeFound = false;
		String searchtags = "";
		long updateTime = System.currentTimeMillis();;
		int count = 0;

		while (cont) {
			line = br.readLine();
			if (line.equalsIgnoreCase(TAG_WALLPAPER_LIST_CLOSE))
				cont = false;
			else {

				if (line.indexOf(TAG_URL) != -1){
					element = getElement(line);
					if (element != null) {
						url = element;
					}
				}
				if (line.indexOf(TAG_TITLE) != -1){
					element = getElement(line);
					if (element != null) {
						title = element;
					}
				}
				if (line.indexOf(TAG_UPDATETIME) != -1){
					element = getElement(line);
					if (element != null) {
						updateTime = Long.valueOf(element);
					}
				}
				if (line.indexOf(TAG_ACTIVE) != -1){
					element = getElement(line);
					if (element != null) {
						active = Boolean.valueOf(element);
					}
				}
				if (line.indexOf(TAG_FAVORITE) != -1){
					element = getElement(line);
					if (element != null) {
						favorite = Boolean.valueOf(element);
					}
				}
				if (line.indexOf(TAG_SEARCHTAGS) != -1){
					element = getElement(line);
					if (element != null) {
						searchtags = element;
					}
				}

				if (line.indexOf(TAG_WALLPAPER_CLOSE) != -1) {
					int numMaxEntries = getMaxRecentEntries();

					if ((favorite == false) && (count < numMaxEntries) ) {
						lstRecentWallpaper.add(new WallpaperEntry(url, title, updateTime, active, searchtags));
						count++;

						if (active)
							activeFound = true;
					}
					if (favorite == true) {
						lstFavoriteWallpaper.add(new WallpaperEntry(url, title, updateTime, active, searchtags));
						count++;

						if (active)
							activeFound = true;
					}        				
					// Initialize possibly unused values with defaults
					updateTime = System.currentTimeMillis();
					title = DEFAULT_NOT_SET;
					url = DEFAULT_NOT_SET;
					active = false;
					favorite = false;
					searchtags = "";
				}
			}
		}

		if (activeFound == false){
			int numEntries = lstRecentWallpaper.size();
			// If no active wallpaper is found we go and repair
			// The last wallpaper is set active (as it was the last one added)
			if (numEntries > 0) {
				lstRecentWallpaper.get(numEntries-1).setActive(true);		
			}
		}

		return true;
	}

	public List<WallpaperEntry> getRecentWallpaperList(){
		return lstRecentWallpaper;
	}

	public List<WallpaperEntry> getFavoriteWallpaperList(){
		return lstFavoriteWallpaper;
	}

	public boolean isFavorite(WallpaperEntry entry){
		int numEntriesFavorite = lstFavoriteWallpaper.size();

		for(int i=0; i < numEntriesFavorite; i++){
			if (lstFavoriteWallpaper.get(i).getUrl().equals(entry.getUrl())){
				return true;
			}
		}

		return false;
	}
		
	public WallpaperEntry setNextImage(Boolean forward) {
		int numEntries = lstRecentWallpaper.size();
		int CurrActive = -1;

		// Search active entry
		for(int i=0; i < numEntries; i++){
			if (lstRecentWallpaper.get(i).getActive()){
				CurrActive = i;
				break;
			}
		}

		Log.d(TAG, "CurrActive:" + CurrActive);

		lstRecentWallpaper.get(CurrActive).setActive(false);

		if (forward) {		
			if (CurrActive+1 >= numEntries) {
				lstRecentWallpaper.get(0).setActive(true);
				return lstRecentWallpaper.get(0);
			} else {
				lstRecentWallpaper.get(CurrActive+1).setActive(true);
				return lstRecentWallpaper.get(CurrActive+1);
			}
		} else {
			// Backward
			if (CurrActive <= 0) {
				lstRecentWallpaper.get(numEntries-1).setActive(true);
				return lstRecentWallpaper.get(numEntries-1);
			} else {
				lstRecentWallpaper.get(CurrActive-1).setActive(true);
				return lstRecentWallpaper.get(CurrActive-1);
			}	
		}
	}

	public WallpaperEntry getActiveWallpaper(){
		int numEntries = lstRecentWallpaper.size();
		WallpaperEntry activeEntry = null;

		// Search active entry
		for(int i=0; i < numEntries; i++){
			if (lstRecentWallpaper.get(i).getActive()){
				activeEntry = lstRecentWallpaper.get(i);
				break;
			}
		}

		if (activeEntry == null) {
			numEntries = this.lstFavoriteWallpaper.size();		
			for(int i=0; i < numEntries; i++){
				if (lstFavoriteWallpaper.get(i).getActive()){
					activeEntry = lstFavoriteWallpaper.get(i);
					break;
				}
			}
		}

		return activeEntry;
	}
	
	public WallpaperEntry searchRecentEntryByUrl(String url){
		int numEntries = lstRecentWallpaper.size();
		WallpaperEntry activeEntry = null;

		// Search active entry
		for(int i=0; i < numEntries; i++){
			if (lstRecentWallpaper.get(i).getUrl().equals(url)){
				return  lstRecentWallpaper.get(i);
			}
		}

		return activeEntry;		
	}

	public void copyToFavorite(WallpaperEntry wallpaper) {
		int numEntries = lstFavoriteWallpaper.size();
		for(int i=0; i < numEntries; i++){
			if (lstFavoriteWallpaper.get(i).getUrl().equalsIgnoreCase(wallpaper.getUrl())){
				// Alredy exists
				return;
			}
		}
		lstFavoriteWallpaper.add(wallpaper);		
	}

	public int getMaxRecentEntries(){
		return parent.getSettingsNumEntries();
	}

	
	public void deleteRecentWallpaper(String url){
		for(Iterator<WallpaperEntry> it = lstRecentWallpaper.iterator(); it.hasNext();){
			if (it.next().getUrl().equalsIgnoreCase(url)) {
				it.remove();
				return;
			}
		}
	}

	public void deleteFavoriteWallpaper(String url){
		for(Iterator<WallpaperEntry> it = lstFavoriteWallpaper.iterator(); it.hasNext();){
			if (it.next().getUrl().equalsIgnoreCase(url)) {
				it.remove();
				return;
			}
		}		
	}
	
	/**
	 *  Search both recent and favorite list for an entry with the same URL.
	 *  If found the entry is marked active, otherwise inactive.
	 * @param entry WallpaperEntry to set active in both lists
	 */
	public void setActiveWallpaper(WallpaperEntry entry){
		WallpaperEntry search;
		for(Iterator<WallpaperEntry> it = lstRecentWallpaper.iterator(); it.hasNext();){
			search = it.next();
			search.setActive(search.getUrl().equalsIgnoreCase(entry.getUrl()));
			// Do not break as we want to set all non matching entries to not active
		}
		
		for(Iterator<WallpaperEntry> it = lstFavoriteWallpaper.iterator(); it.hasNext();){
			search = it.next();
			search.setActive(search.getUrl().equalsIgnoreCase(entry.getUrl()));
			// Do not break as we want to set all non matching entries to not active
		}
	}

	//----------------------------------------
	// Wallpaper tag handling
	//----------------------------------------
	
	public String[] geFourtWallpaperByTag(String tags) {
		String[] files = {null,null, null, null};
		int numRecent = getNumTagsRecent(tags);
		int numFavorite = getNumTagsFavorite(tags);
		int sum = numRecent + numFavorite;
		
		Log.d(TAG, "numRecent: " + numRecent + ", numFavorite: " + numFavorite);
		
		int maxPics = Math.min(sum, 4);
		
		for (int i=0; i<maxPics; i++) {
			Random rand = new Random();
			int myRandom = rand.nextInt(sum);
			//Log.d(TAG, "rand: " + myRandom);
			
			if (myRandom < numRecent-1) {
				// get from recent list
				files[i] = lstRecentWallpaper.get(myRandom).getFileName();
			} else {
				// get from favorite list
				files[i] = lstRecentWallpaper.get(myRandom-numRecent).getFileName();
			}
			
			//Log.d(TAG, "file: " + files[i]);
		}
		
		return files;
	}
	
	
	private int getNumTagsFavorite(String tags){
		int num = 0;
		for(Iterator<WallpaperEntry> it = lstFavoriteWallpaper.iterator(); it.hasNext();){
			if (it.next().getSearchtags().equalsIgnoreCase(tags)) {
				num++;
			}
		}
		return num;
	}

	private int getNumTagsRecent(String tags){
		int num = 0;
		for(Iterator<WallpaperEntry> it = lstRecentWallpaper.iterator(); it.hasNext();){
			if (it.next().getSearchtags().equalsIgnoreCase(tags)) {
				num++;
			}
		}
		return num;
	}
}
