package de.qbert.wallpaper.doc;

public class DocBaseEntry {

	public DocBaseEntry(){
	}

	public boolean isTag(String line, String tag) {		
		if (line.indexOf(tag) != -1)
			return true;
		else
			return false;
	}
	
	public String getElement(String line){
		int pos1, pos2;
		String ret = null;
		
		pos1 = line.indexOf(">");
		if ( (pos1 != -1) && (pos1+2 < line.length()) ) {
			pos2 = line.indexOf("<", pos1+1);
			if (pos2 != -1) {
				ret = line.substring(pos1+1, pos2);
			}
		}
		return ret;
	}	
}
