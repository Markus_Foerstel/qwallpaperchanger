package de.qbert.wallpaper.doc;

import android.annotation.SuppressLint;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class DocEntrySliceDB extends DocBaseEntry implements DocBaseInterface {

	private static String INDENT = "   ";
	
	public static final String TAG_SLICE_DB = "<SliceDB>";
	public static final String TAG_SLICE_DB_CLOSE = "</SliceDB>";

	private static final String TAG_SLICE_LIST = "<SliceList>";
	private static final String TAG_SLICE_LIST_CLOSE = "</SliceList>";
	
	private static final String TAG_SLICE_ENTRY = "<SliceEntry>";
	private static final String TAG_SLICE_ENTRY_CLOSE = "</SliceEntry>";
	private static final String TAG_STATE = "<State>";
	private static final String TAG_SEARCH_STRING = "<SearchString>";
	private static final String TAG_MILLIS = "<Millis>";
	private static final String TAG_DATE = "<Date>";	// Same as Millis but in a readable format
	private static final String TAG_TOTAL = "<Total>";
	
	// Default values
	private static final String DEF_SEARCH_STRING = "--Nothing--";
	private static final int DEF_STATE = 0;
	
	// Backup of actual search String. 
	// Backup is needed to detect if a new search has started.
	private String searchString;

	public String getSearchString(){
		return searchString;
	}
	
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	private int state;
	
	public int getState(){
		return state;
	}
	
	public void setState(int state) {
		this.state = state;
	}
	
	List<Slice> lstSlices;
	
	public class Slice{
		private long millis;
		private int total;
	
		public Slice(long millis, int total){
			this.millis = millis;
			this.total = total;
		}
		
		public long getMillis(){
			return millis;
		}
		
		public int getTotal(){
			return total;
		}
	}
	
	public void addSlice(long millis, int total) {
		lstSlices.add(new Slice(millis, total));
	}

	public List<Slice> getSliceList() {
		return lstSlices;
	}

	
	public DocEntrySliceDB() {
		lstSlices = new ArrayList<Slice>();
		setState(DEF_STATE);
		setSearchString(DEF_SEARCH_STRING);
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public boolean storeElement(BufferedWriter bw) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		StringBuilder builder = new StringBuilder();

		builder.append(TAG_SLICE_DB + System.getProperty("line.separator"));

		builder.append(INDENT + TAG_STATE + Integer.toString(getState()) + "</>"  + System.getProperty("line.separator"));
		builder.append(INDENT + TAG_SEARCH_STRING + getSearchString() + "</>"  + System.getProperty("line.separator"));

		builder.append(INDENT + TAG_SLICE_LIST + System.getProperty("line.separator"));

		for(Iterator<Slice> it = lstSlices.iterator(); it.hasNext();){
			Slice slice = it.next();	
			builder.append(INDENT + INDENT + TAG_SLICE_ENTRY + System.getProperty("line.separator"));
			builder.append(INDENT + INDENT + TAG_MILLIS + Long.toString(slice.getMillis()) + "</>"  + System.getProperty("line.separator"));
			
			builder.append(INDENT + INDENT + TAG_DATE + sdf.format(slice.getMillis()) + "</>"  + System.getProperty("line.separator"));
			
			builder.append(INDENT + INDENT + TAG_TOTAL + Integer.toString(slice.getTotal()) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + INDENT + TAG_SLICE_ENTRY_CLOSE  + System.getProperty("line.separator"));
		}

		builder.append(INDENT + TAG_SLICE_LIST_CLOSE  + System.getProperty("line.separator"));		
		builder.append(TAG_SLICE_DB_CLOSE + System.getProperty("line.separator"));

		bw.write(new String(builder));

		return true;
	}

	@Override
	public boolean loadElement(BufferedReader br) throws IOException {
		boolean cont = true;
		String line;
		String element;
		String searchString = "";
		long millis = 0;
		int total = 0;
		
        while (cont) {
        	line = br.readLine();
        	if (line == null)
        		cont = false;
        	else if (line.equalsIgnoreCase(TAG_SLICE_DB_CLOSE))
        		cont = false;
        	else {
        		if (line.indexOf(TAG_SEARCH_STRING) != -1){
           			element = getElement(line);
        			if (element != null) {
        				searchString = element;
        			}
        		}
        		if (line.indexOf(TAG_STATE) != -1){
           			element = getElement(line);
        			if (element != null) {
        				state = Integer.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_MILLIS) != -1){
           			element = getElement(line);
        			if (element != null) {
        				millis = Long.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_TOTAL) != -1){
           			element = getElement(line);
        			if (element != null) {
        				total = Integer.valueOf(element);
        			}
        		}

        		if (line.indexOf(TAG_SLICE_ENTRY_CLOSE) != -1){
        			this.setSearchString(searchString);
        			lstSlices.add(new Slice(millis, total));
        		}  	
        	}
        }
        
//        // Create default entry if no time is found
//        if (lstEvents.size() == 0) {
//        	setEvent(18, 0, true, true, true,true, true, true, true);
//        }

		return true;
	}

}
