package de.qbert.wallpaper.doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.util.Log;

public class DocEntrySearch extends DocBaseEntry implements DocBaseInterface {

	private static final String TAG="Wallpaper";
	
	private static final String DEFAULT_SEARCH_STRING = "Wallpaper";
	
	private static String INDENT = "   ";
	
	public static final String TAG_SEARCH = "<Search>";
	public static final String TAG_SEARCH_CLOSE = "</Search>";

	private static final String TAG_SEARCH_ENTRY = "<SearchEntry>";
	private static final String TAG_SEARCH_ENTRY_CLOSE = "</SearchEntry>";

	private static final String TAG_ACTIVE = "<active>";
	private static final String TAG_TEXT = "<text>";
	private static final String TAG_URL = "<url>";
	private static final String TAG_NUMBER = "<number>";

	public class SearchEntry{

		private Boolean active;
		private String searchText;
		private String url[];
		private int number;
		
		public SearchEntry(Boolean active, String searchText, String[] url, int number) {
			super();
			this.active = active;
			this.searchText = searchText;
			this.url = url;
			this.number = number;
		}
		
		/**
		 * @return the active
		 */
		public Boolean isActive() {
			return active;
		}
		/**
		 * @param active the active to set
		 */
		public void setActive(Boolean active) {
			this.active = active;
		}
		/**
		 * @return the searchText
		 */
		public String getSearchText() {
			return searchText;
		}
		/**
		 * @param searchText the searchText to set
		 */
		public void setSearchText(String searchText) {
			this.searchText = searchText;
		}
		/**
		 * @return the number
		 */
		public int getNumber() {
			return number;
		}
		/**
		 * @param number the number to set
		 */
		public void setNumber(int number) {
			this.number = number;
		}
		/**
		 * @return the url
		 */
		public String[] getUrl() {
			return url;
		}
		/**
		 * @param url the url to set
		 */
		public void setUrl(String url[]) {
			this.url = url;
		}
	}
	
	private List<SearchEntry> searchEntryArray;
	
//	private String searchText;
//	private int number;
	
	public DocEntrySearch(DocBase parent) {
		super();
		searchEntryArray = new ArrayList<SearchEntry>();
	}
	
	@Override
	public boolean storeElement(BufferedWriter bw)
			throws IOException {
      
      StringBuilder builder = new StringBuilder();
      builder.append(TAG_SEARCH + System.getProperty("line.separator"));
      
		for(Iterator<SearchEntry> it = searchEntryArray.iterator(); it.hasNext();){
			SearchEntry entry = it.next();
			builder.append(INDENT + TAG_SEARCH_ENTRY + System.getProperty("line.separator"));
			
			builder.append(INDENT + TAG_ACTIVE + Boolean.toString(entry.isActive()) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_TEXT + entry.getSearchText() + "</>"  + System.getProperty("line.separator"));			
			builder.append(INDENT + TAG_NUMBER + Integer.toString(entry.getNumber()) + "</>"  + System.getProperty("line.separator"));

			String url[] = entry.getUrl();
			for (int i=0; i<url.length; i++){
				if (url[i] != null) {
					builder.append(INDENT + TAG_URL + url[i] + "</>"  + System.getProperty("line.separator"));
				}
			}
			
			builder.append(TAG_SEARCH_ENTRY_CLOSE  + System.getProperty("line.separator"));
		}
      
      builder.append(TAG_SEARCH_CLOSE  + System.getProperty("line.separator"));

      bw.write(new String(builder));

      return true;
	}


	@Override
	public boolean loadElement(BufferedReader br) 
			throws IOException {
	
		int numEntries = 0;
		boolean cont = true;
		String line;
		String element;
		
		boolean active = true;
		String searchText = "wallpaper";
		int urlNumber = 0;
		String url[] = new String[]{null, null, null, null};
		int number = -1;
		
        while (cont) {
        	line = br.readLine();
        	if (line.equalsIgnoreCase(TAG_SEARCH_CLOSE)) {
        		
        		// backwards compatibility
        		if (numEntries == 0){
        			SearchEntry se = new SearchEntry(active, searchText, url, number);
        			searchEntryArray.add(se);
        		}
        		
        		cont = false;
        	}
        	else {
        		if (line.indexOf(TAG_SEARCH_ENTRY) != -1){
        			urlNumber = 0;
        			for (int i=0; i<url.length; i++){
        				url[i] = null;
        			}
        		}
        		if (line.indexOf(TAG_SEARCH_ENTRY_CLOSE) != -1){
        			
        			SearchEntry se = new SearchEntry(active, searchText, url, number);
        			searchEntryArray.add(se);
        			
        			numEntries++;
        		}
        		if (line.indexOf(TAG_ACTIVE) != -1){
        			element = getElement(line);
        			if (element != null) {
        				active = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_TEXT) != -1){
        			element = getElement(line);
        			if (element != null) {
        				searchText = element;
        			}
        		}
        		if (line.indexOf(TAG_NUMBER) != -1){
        			element = getElement(line);
        			if (element != null) {
        				number = Integer.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_URL) != -1){
        			element = getElement(line);
        			if (element != null) {
        				url[urlNumber++] = element;
        			}
        		}
        	}
            
        }
        return true;
	}

	public List<SearchEntry> getTagList(){
		return searchEntryArray;
	}
	
	public void deleteSearchText(int position) {
		
		if (searchEntryArray.size() <= 1)
			return;
		
		int pos = 0;
		SearchEntry seLast = null;
		boolean deleteIsActive;
		
		for(Iterator<SearchEntry> it = searchEntryArray.iterator(); it.hasNext();){
			boolean activeSet = false;
			SearchEntry se = it.next();
			
			if (pos == position) {
				
				deleteIsActive = se.isActive();
				
				// Set list entry active before remove position if exists (seLast != null)
				if ( (deleteIsActive) && (seLast != null) ) {
					seLast.setActive(true);
					activeSet = true;
				}
				
				it.remove();
				
				// If no new entry is set active set entry following remove position to active.
				if ( (deleteIsActive) &&  (it.hasNext()) && (!activeSet) ) {
					 se = it.next();
					se.setActive(true);
				}
				
				return;
			}
			
			seLast = se;
			
			pos++;
		}
		
	}
	
	public boolean isActiveSearchText(int position) {
		return searchEntryArray.get(position).isActive();
	}

	public String getSearchText() {
		int index = getActiveEntry();
		if (index != -1)
			return searchEntryArray.get(index).getSearchText();
		else
		{
			Log.d(TAG, "Search Index not found. Use default");
			return DEFAULT_SEARCH_STRING;
		}
	}
	
	/**
	 * Store the searchtext as space separated tags in the document
	 * Spaces and special characters are removed
	 * 
	 * @param searchText
	 */
	public void setSearchText(String searchText) {
		int index = searchTextExists(searchText);
		
		removeAllActiveFlasgs();
		
		if (index == -1) {
			String url[] = new String[] {null, null, null, null};
			
			searchText = searchText.trim();
			
			searchText = searchText.replaceAll("[<>]+","");
			
			searchText = searchText.replaceAll(" +", " ");
			
			SearchEntry se = new SearchEntry(true /* active */, searchText, url , 0);
			searchEntryArray.add(se);
		} else {
			// Change to existing entry
			searchEntryArray.get(index).setActive(true);
		}
	}

	public int getNumber() {
		int index = getActiveEntry();
		if (index != -1)
			return searchEntryArray.get(index).getNumber();
		else
			return 0;
	}

	public void setNumber(int number) {
		int index = getActiveEntry();
		if (index != -1)
			searchEntryArray.get(index).setNumber(number);
	}
	
	public void setActive(int number) {
		if ( (number >= 0) && (number < searchEntryArray.size()) ) {
			removeAllActiveFlasgs();
			searchEntryArray.get(number).setActive(true);
		}
	}
	
	// ----------------------------------------
	// Helper functions
	// ----------------------------------------
	
	private int getActiveEntry(){
		for(int i=0; i < searchEntryArray.size(); i++){
			if (searchEntryArray.get(i).active){
				return i;
			}
		}
		return -1;
	}
	
	private int searchTextExists(String searchText){
		for(int i=0; i < searchEntryArray.size(); i++){
			if (searchEntryArray.get(i).getSearchText().equals(searchText)){
				return i;
			}
		}
		return -1;
	}
	
	private void removeAllActiveFlasgs(){
		for(int i=0; i < searchEntryArray.size(); i++){
			searchEntryArray.get(i).setActive(false);
		}
	}
	
}
