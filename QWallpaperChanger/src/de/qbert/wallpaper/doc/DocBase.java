package de.qbert.wallpaper.doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.Manifest;
import android.app.Activity;
import android.os.Environment;
import android.util.Log;
import de.qbert.wallpaper.doc.DocEntrySearch.SearchEntry;
import de.qbert.wallpaper.doc.DocEntrySliceDB.Slice;
import de.qbert.wallpaper.doc.DocEntryTime.Event;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;

public class DocBase {

	private static DocBase instance = null;
	
	public static final String TAG="QWallpaperDoc";

	private final String FILENAME = "document_v2.xml";
	private final String DOC_VERSION = "0.2";
	
	private static final String BASE_FOLDER = "Wallpaper";
	private static final String RECENT_FOLDER = "Recent";
	private static final String STORAGE_FOLDER = "Store";
	
	private static final String TAG_DOC_START = "<Document>"; 
	private static final String TAG_DOC_CLOSE = "</Document>";
	
	public static final String MAX_RECENT_ENTRIES_DEF = "10";
	public static final Boolean MAX_WIFI_ONLY_DEF = false;
	public static final Boolean MAX_WLAN_ONLY_DEF = false;

	private DocEntryTime m_docEntryTime;
	private DocEntrySearch m_docEntrySearch;
	private DocEntryWallpaper m_docEntryWallpaper;
	private DocEntrySettings m_docEntrySettings;
	private DocEntrySliceDB m_docEntrySliceDB;
	
	private String lastError;
			
	//--------------------------------------------
	// Constructor
	//--------------------------------------------

	public static DocBase getInstance() {
		if(instance == null) {
			instance = new DocBase();
			instance.init();
		}
		return instance;
	}
	
	private DocBase(){
	}
	   
	private void init(){
		setLastError(null);
                
		// Create document base folder
		File folderBase = new File(getDocBaseFolder());
		boolean success = true;
		if (!folderBase.exists()) {
			//Toast.makeText(MainActivity.this, "Directory Does Not Exist, Create It", Toast.LENGTH_SHORT).show();
			success = folderBase.mkdir();	
		}
		if (success) {
			//Toast.makeText(MainActivity.this, "Directory Created", Toast.LENGTH_SHORT).show();
		} else {
			//Toast.makeText(MainActivity.this, "Failed - Error", Toast.LENGTH_SHORT).show();
		}
		
		File folderRecent = new File(getDocRecentFolder());
		success = true;
		if (!folderRecent.exists()) {
			//Toast.makeText(MainActivity.this, "Directory Does Not Exist, Create It", Toast.LENGTH_SHORT).show();
			success = folderRecent.mkdir();	
		}
		if (success) {
			//Toast.makeText(MainActivity.this, "Directory Created", Toast.LENGTH_SHORT).show();
		} else {
			//Toast.makeText(MainActivity.this, "Failed - Error", Toast.LENGTH_SHORT).show();
		}

		File folderStore = new File(getDocStoreFolder());
		success = true;
		if (!folderStore.exists()) {
			//Toast.makeText(MainActivity.this, "Directory Does Not Exist, Create It", Toast.LENGTH_SHORT).show();
			success = folderStore.mkdir();	
		}
		if (success) {
			//Toast.makeText(MainActivity.this, "Directory Created", Toast.LENGTH_SHORT).show();
		} else {
			//Toast.makeText(MainActivity.this, "Failed - Error", Toast.LENGTH_SHORT).show();
		}
				
		m_docEntrySettings = new DocEntrySettings();
		m_docEntryTime = new DocEntryTime();
		m_docEntrySearch = new DocEntrySearch(this);
		m_docEntryWallpaper = new DocEntryWallpaper(this);
		m_docEntrySliceDB = new DocEntrySliceDB();
			
		if (!loadDocument()) {
			if (!storeDocument())
				setLastError("ERROR (0): Loading document failed");
		} else {			
			setLastError(null);
		}
	}

	//--------------------------------------------
	// Methods
	//--------------------------------------------

	//--------------------------------------------
	// TIME Functions
	//--------------------------------------------

	public void setEvent(int hour, int minute,
			              boolean mon, boolean tue, boolean wed,
			              boolean thu, boolean fri, boolean sat,
			              boolean sun) {
		m_docEntryTime.setEvent(hour,  minute,
	               mon,  tue,  wed,
	               thu,  fri,  sat,
	               sun);
	}
	
	public void setEventInActive(){
		m_docEntryTime.setInActive();
	}
	
	public Event getEvent() {
			return m_docEntryTime.getEvent();
	}
	
	public String getDocBaseFolder(){
		//return getExternalFilesDir(null) + "/" + BASE_FOLDER;
		String str = Environment.getExternalStorageState();
		return Environment.getExternalStorageDirectory() + File.separator + BASE_FOLDER;
	}
	
	public String getDocRecentFolder(){
		return Environment.getExternalStorageDirectory() + File.separator + BASE_FOLDER + File.separator + RECENT_FOLDER;
	}

	public String getDocStoreFolder(){
		return Environment.getExternalStorageDirectory() + File.separator + BASE_FOLDER + File.separator + STORAGE_FOLDER;
	}

	//--------------------------------------------
	// Search functions
	//--------------------------------------------

	public List<SearchEntry> getTagList(){
		return m_docEntrySearch.getTagList();
	}
	
	public void setTotal(int total){
		m_docEntrySearch.setNumber(total);
	}

	public int getTotal(){
		return m_docEntrySearch.getNumber();
	}
	
	public void setSearchString(String strSearchString){
		m_docEntrySearch.setSearchText(strSearchString);
		Log.d(TAG, "setSearchString:" + strSearchString);
	}
	
	public String getSearchString(){
		Log.d(TAG, "getSearchString:" + m_docEntrySearch.getSearchText());
		return m_docEntrySearch.getSearchText();
	}
	
	public void deleteSearchText(int position){
		m_docEntrySearch.deleteSearchText(position);
	}
	
	public boolean isActiveSearchText(int position) {
		return m_docEntrySearch.isActiveSearchText(position);
	}
	
	public void setActive(int number) {
		m_docEntrySearch.setActive(number);
	}
	
	//--------------------------------------------
	// Wallpaper functions
	//--------------------------------------------
	
	public void addWallpaper(String url, String title, String searchtags) {
		m_docEntryWallpaper.addWallpaperEntry(url, title, searchtags);
	}
	
	public List<WallpaperEntry> getRecentWallpaperList(){
		return m_docEntryWallpaper.getRecentWallpaperList();
	}

	public List<WallpaperEntry> getFavoriteWallpaperList(){
		return m_docEntryWallpaper.getFavoriteWallpaperList();
	}

	public void deleteRecentWallpaper(String url){
		 m_docEntryWallpaper.deleteRecentWallpaper(url);
	}

	public void deleteFavoriteWallpaper(String url){
		 m_docEntryWallpaper.deleteFavoriteWallpaper(url);
	}
	
	public String[] geFourtWallpaperByTag(String tags){
		 return m_docEntryWallpaper.geFourtWallpaperByTag(tags);
	}

	public boolean storeDocument(){
		try {
			File file = new File(getDocBaseFolder() + "/" + FILENAME);

			// If file does not exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			// Store version information
			storeLeadIn(bw);

			// Store content
			m_docEntrySettings.storeElement(bw);
			m_docEntryTime.storeElement(bw);
			m_docEntrySearch.storeElement(bw);
			m_docEntryWallpaper.storeElement(bw);
			m_docEntrySliceDB.storeElement(bw);

			// Store lead out
			storeLeadOut(bw);

			bw.close();
			fw.close();

			setLastError(null);

			return true;

		} catch (IOException e) {
			setLastError("ERROR (1): " + e.getMessage());
			e.printStackTrace();
			return false;
		}	
	}

	private void storeLeadIn(BufferedWriter bw) throws IOException
	{
		StringBuilder builder = new StringBuilder();
		builder.append(TAG_DOC_START + System.getProperty("line.separator"));
		builder.append("<version " + DOC_VERSION + "</>"  + System.getProperty("line.separator"));

		bw.write(new String(builder));
	}

	private void storeLeadOut(BufferedWriter bw) throws IOException
	{
		bw.write(TAG_DOC_CLOSE  + System.getProperty("line.separator"));
	}

	public boolean loadDocument(){
		boolean ret = true;

		File file = new File(getDocBaseFolder() + "/" + FILENAME);

		FileReader fr = null;
		BufferedReader br = null;
		
		try {
			fr = new FileReader(file.getAbsoluteFile());
			br = new BufferedReader(fr);
			
			String line;

			// Skip version information

			do {
				// Load content
	        	line = br.readLine();
	        	if (line.equalsIgnoreCase(DocEntrySettings.TAG_SETTINGS)) {
	        		m_docEntrySettings.loadElement(br);
	        	}
	
	        	if (line.equalsIgnoreCase(DocEntryTime.TAG_TIME_LIST)) {
	        		m_docEntryTime.loadElement(br);
	        	}
	
	        	if (line.equalsIgnoreCase(DocEntrySearch.TAG_SEARCH)) {
	        		m_docEntrySearch.loadElement(br);
	        	}
	
	        	if (line.equalsIgnoreCase(DocEntryWallpaper.TAG_WALLPAPER_LIST)) {
	        		m_docEntryWallpaper.loadElement(br);
	        	}
	        	if (line.equalsIgnoreCase(DocEntrySliceDB.TAG_SLICE_DB)) {
	        		m_docEntrySliceDB.loadElement(br);
	        	}
			} while (line.indexOf(TAG_DOC_CLOSE) == -1);

			//Skipping lead out

			br.close();
			fr.close();
			
			setLastError(null);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			setLastError("ERROR (2): " + e.getMessage());
			ret = false;
		} catch (IOException e) {
			e.printStackTrace();
			setLastError("ERROR (3): " + e.getMessage());
			ret = false;
		}

		return ret;
	}

	public String getName(String url){
		int pos = url.lastIndexOf("/");
		return url.substring(pos+1);
	}
	
	public WallpaperEntry setNextImage(Boolean forward){
		return m_docEntryWallpaper.setNextImage(forward);
	}
		
	public WallpaperEntry getActiveWallpaper(){
		return m_docEntryWallpaper.getActiveWallpaper();
	}

	public boolean isFavorite(WallpaperEntry entry){
		return m_docEntryWallpaper.isFavorite(entry);		
	}


	public String getActiveWallpaperTitle(){
		WallpaperEntry entry = m_docEntryWallpaper.getActiveWallpaper();
		if (entry == null){
			return null;
		} else {
			return entry.getTitle();
		}
	}
	
	public void copyToStorage(String Url) throws IOException{
		File src = new File(getDocRecentFolder() + "/" + getName(Url));
		File dst = new File(getDocStoreFolder() + "/" + getName(Url));
		
	    InputStream in = new FileInputStream(src);
	    OutputStream out = new FileOutputStream(dst);

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}

	/**
	 * Set current active wallpaper as favorite.
	 * ATTENTION: This function is called from the Widget only. The Toast was removed.
	 * This means:
	 *   Copy image to favorite 
	 *   Set Favorite
	 */
	public void setFavorite(){
		WallpaperEntry activeEntry= m_docEntryWallpaper.getActiveWallpaper();
				
		if (activeEntry != null){
			try{
				copyToStorage(activeEntry.getUrl());
				m_docEntryWallpaper.copyToFavorite(activeEntry);
				
				setLastError(null);
			}
			catch(IOException e){
				setLastError("ERROR (4): " + e.getMessage());
			}
		}
	}

	/**
	 * Set passed in recent wallpaper as favorite.
	 * ATTENTION: This function is called from the Application Activity only. The Toast was removed.
	 * @param recentEntry
	 */
	public void setFavorite(WallpaperEntry entry){
				
		if (entry != null){
			try{
				copyToStorage(entry.getUrl());
				m_docEntryWallpaper.copyToFavorite(entry);
				
				setLastError(null);
			}
			catch(IOException e){
				setLastError("ERROR (5): " + e.getMessage());
			}
		}
	}
	
	
	//--------------------------------------------
	// Settings  functions
	//--------------------------------------------
	
	public WallpaperEntry searchRecentEntryByUrl(String url){
		return m_docEntryWallpaper.searchRecentEntryByUrl(url);
	}
	
	public int getSettingsNumEntries(){
		return m_docEntrySettings.getNumEntries();
	}

	public void setSettingsNumEntries(int numEntries){
		m_docEntrySettings.setNumEntries(numEntries);
	}
	
	public void setOnlyWiFi(boolean wifiOnly){
		m_docEntrySettings.setOnlyWiFi(wifiOnly);
	}

	public void setOnlyWLan(boolean WLanOnly){
		m_docEntrySettings.setOnlyWiFi(WLanOnly);
	}	

	public void setActiveWallpaper(WallpaperEntry entry){
		m_docEntryWallpaper.setActiveWallpaper(entry);
	}
	
	//--------------------------------------------
	// Slice DB functions
	//--------------------------------------------

	public DocEntrySliceDB getSliceDB() {
		return m_docEntrySliceDB;
	}
	
	public int getSliceState(){
		return m_docEntrySliceDB.getState();
	}
	
	public void setSliceState(int state) {
		m_docEntrySliceDB.setState(state);
	}
	
	public void addSlice(long millis, int total) {
		m_docEntrySliceDB.addSlice(millis, total);
	}

	public List<Slice> getSliceList() {
		return m_docEntrySliceDB.getSliceList();
	}

	
	public String getLastError() {
		return lastError;
	}

	private void setLastError(String lastError) {
		this.lastError = lastError;
	}
	

}
