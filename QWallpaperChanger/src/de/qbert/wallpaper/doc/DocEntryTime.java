package de.qbert.wallpaper.doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DocEntryTime extends DocBaseEntry implements DocBaseInterface {

	private static String INDENT = "   ";
	
	public static final String TAG_TIME_LIST = "<TimeList>";
	public static final String TAG_TIME_LIST_CLOSE = "</TimeList>";
	private static final String TAG_TIME = "<Time>";
	private static final String TAG_TIME_CLOSE = "</Time>";
	private static final String TAG_HOUR = "<Hour>";
	private static final String TAG_MINUTE = "<Minute>";
	private static final String TAG_MONDAY = "<Monday>";
	private static final String TAG_TUESDAY = "<Tuesday>";
	private static final String TAG_WEDNESDAY = "<Wednesday>";
	private static final String TAG_THURSDAY = "<Thursday>";
	private static final String TAG_FRIDAY = "<Friday>";
	private static final String TAG_SATURDAY = "<Saturday>";
	private static final String TAG_SUNDAY = "<Sunday>";
	
	private static final int HOUR_DEF = 99;
	private static final int MINUTE_DEF = 99;
	private static final boolean DAY_DEF = false;
	
	List<Event> lstEvents;
	
	public class Event{
		private int hour;
		private int minute;
		private boolean monday;
		private boolean tuesday;
		private boolean wednesday;
		private boolean thursday;
		private boolean friday;
		private boolean saturday;
		private boolean sunday;
		
		public Event(int hour, int minute,
				boolean monday, boolean tuesday, boolean wednesday, 
				boolean thursday, boolean friday, boolean saturday, 
				boolean sunday) {
			this.hour = hour;
			this.minute = minute;
			this.monday = monday;
			this.tuesday = tuesday;
			this.wednesday = wednesday;
			this.thursday = thursday;
			this.friday = friday;
			this.saturday = saturday;
			this.sunday = sunday;
		}
		
		public boolean isActive(){
			return ( (monday == true) || (tuesday == true) || (wednesday == true) || 
					  (thursday == true) || (friday == true) || (saturday == true) || (sunday == true) );
		}
		
		public int getHour() {
			return hour;
		}

		public void setHour(int hour) {
			this.hour = hour;
		}

		public int getMinute() {
			return minute;
		}

		public void setMinute(int minute) {
			this.minute = minute;
		}

		public boolean isMonday() {
			return monday;
		}

		public void setMonday(boolean monday) {
			this.monday = monday;
		}

		public boolean isTuesday() {
			return tuesday;
		}

		public void setTuesday(boolean tuesday) {
			this.tuesday = tuesday;
		}

		public boolean isWednesday() {
			return wednesday;
		}

		public void setWednesday(boolean wednesday) {
			this.wednesday = wednesday;
		}

		public boolean isThursday() {
			return thursday;
		}

		public void setThursday(boolean thursday) {
			this.thursday = thursday;
		}

		public boolean isFriday() {
			return friday;
		}

		public void setFriday(boolean friday) {
			this.friday = friday;
		}

		public boolean isSaturday() {
			return saturday;
		}

		public void setSaturday(boolean saturday) {
			this.saturday = saturday;
		}

		public boolean isSunday() {
			return sunday;
		}

		public void setSunday(boolean sunday) {
			this.sunday = sunday;
		}
		
	}
	
	public DocEntryTime() {
		lstEvents = new ArrayList<Event>();
	}
	
	public void setEvent(int hour, int minute,
            boolean mon, boolean tue, boolean wed,
            boolean thu, boolean fri, boolean sat,
            boolean sun) {
		lstEvents.clear();
		Event event = new Event(hour,  minute,
	               mon,  tue,  wed,
	               thu,  fri,  sat,
	               sun);
		lstEvents.add(event);
	}
	
	public Event getEvent(){
		if (lstEvents.size() == 0) {
			setEvent(0, 0, false, false, false, false, false, false, false);
		}
		return lstEvents.get(0);
	}
	
	public void setInActive(){
		setEvent(0, 0, false, false, false, false, false, false, false);
	}
	
	@Override
	public boolean storeElement(BufferedWriter bw)
			throws IOException {

		StringBuilder builder = new StringBuilder();
		builder.append(TAG_TIME_LIST + System.getProperty("line.separator"));
		
		for(Iterator<Event> it = lstEvents.iterator(); it.hasNext();){
			Event entry = it.next();	
			builder.append(INDENT + TAG_TIME + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_HOUR + Integer.toString(entry.hour) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_MINUTE + Integer.toString(entry.minute) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_MONDAY + Boolean.toString(entry.monday) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_TUESDAY + Boolean.toString(entry.tuesday) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_WEDNESDAY + Boolean.toString(entry.wednesday) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_THURSDAY + Boolean.toString(entry.thursday) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_FRIDAY + Boolean.toString(entry.friday) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_SATURDAY + Boolean.toString(entry.saturday) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_SUNDAY + Boolean.toString(entry.sunday) + "</>"  + System.getProperty("line.separator"));
			builder.append(INDENT + TAG_TIME_CLOSE  + System.getProperty("line.separator"));
		}

		builder.append(TAG_TIME_LIST_CLOSE  + System.getProperty("line.separator"));		

		bw.write(new String(builder));

		return true;
	}

	@Override
	public boolean loadElement(BufferedReader br) 
			throws IOException {
	
		boolean cont = true;
		String line;
		String element;
		int hour = HOUR_DEF;
		int minute = MINUTE_DEF;
		boolean monday = DAY_DEF;
		boolean tuesday = DAY_DEF;
		boolean wednesday = DAY_DEF;
		boolean thursday = DAY_DEF;
		boolean friday = DAY_DEF;
		boolean saturday = DAY_DEF;
		boolean sunday = DAY_DEF;
		
        while (cont) {
        	line = br.readLine();
        	if (line == null)
        		cont = false;
        	else if (line.equalsIgnoreCase(TAG_TIME_LIST_CLOSE))
        		cont = false;
        	else {
        		if (line.indexOf(TAG_HOUR) != -1){
           			element = getElement(line);
        			if (element != null) {
        				hour = Integer.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_MINUTE) != -1){
           			element = getElement(line);
        			if (element != null) {
        				minute = Integer.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_MONDAY) != -1){
           			element = getElement(line);
        			if (element != null) {
        				monday = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_TUESDAY) != -1){
           			element = getElement(line);
        			if (element != null) {
        				tuesday = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_WEDNESDAY) != -1){
           			element = getElement(line);
        			if (element != null) {
        				wednesday = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_THURSDAY) != -1){
           			element = getElement(line);
        			if (element != null) {
        				thursday = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_FRIDAY) != -1){
           			element = getElement(line);
        			if (element != null) {
        				friday = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_SATURDAY) != -1){
           			element = getElement(line);
        			if (element != null) {
        				saturday = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_SUNDAY) != -1){
           			element = getElement(line);
        			if (element != null) {
        				sunday = Boolean.valueOf(element);
        			}
        		}
        		if (line.indexOf(TAG_TIME_CLOSE) != -1){
        			lstEvents.add(new Event(hour, minute, monday, tuesday, wednesday, thursday, friday, saturday, sunday));
        			hour = HOUR_DEF;
        			minute = MINUTE_DEF;
        			monday = DAY_DEF;
        			tuesday = DAY_DEF;
        			wednesday = DAY_DEF;
        			thursday = DAY_DEF;
        			friday = DAY_DEF;
        			saturday = DAY_DEF;
        			sunday = DAY_DEF;
        		}  	
        	}
        }
        
        // Create default entry if no time is found
        if (lstEvents.size() == 0) {
        	setEvent(18, 0, true, true, true,true, true, true, true);
        }
        
        return true;
	}

}
