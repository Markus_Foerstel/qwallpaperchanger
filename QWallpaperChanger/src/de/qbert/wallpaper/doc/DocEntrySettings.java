package de.qbert.wallpaper.doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class DocEntrySettings extends DocBaseEntry implements DocBaseInterface {

	//private static final String TAG="Wallpaper";
	private static String INDENT = "   ";
	
	public static final String TAG_SETTINGS = "<Settings>";
	public static final String TAG_SETTINGS_CLOSE = "</Settings>";
	private static final String TAG_NUM_ENTRIES = "<numEntries>";
	private static final String TAG_ONLY_WIFI = "<onlyWiFi>";
	private static final String TAG_ONLY_WLAN = "<onlyLAN>";
	
	private int numEntries;
	private boolean onlyWiFi;
	private boolean onlyWLAN;
	
	public DocEntrySettings() {
		numEntries = 10;
		onlyWiFi = false;
		onlyWLAN = false;
	}

	public int getNumEntries() {
		return numEntries;
	}


	public void setNumEntries(int numEntries) {
		this.numEntries = numEntries;
	}


	public boolean isOnlyWiFi() {
		return onlyWiFi;
	}


	public void setOnlyWiFi(boolean onlyWiFi) {
		this.onlyWiFi = onlyWiFi;
	}


	public boolean isOnlyWLAN() {
		return onlyWLAN;
	}


	public void setOnlyWLAN(boolean onlyWLAN) {
		this.onlyWLAN = onlyWLAN;
	}


	@Override
	public boolean storeElement(BufferedWriter bw) throws IOException {
		StringBuilder builder = new StringBuilder();
		builder.append(TAG_SETTINGS + System.getProperty("line.separator"));
		builder.append(INDENT + TAG_ONLY_WIFI + Boolean.toString(this.isOnlyWiFi()) + "</>"  + System.getProperty("line.separator"));
		builder.append(INDENT + TAG_ONLY_WLAN + Boolean.toString(this.isOnlyWLAN()) + "</>"  + System.getProperty("line.separator"));
		builder.append(TAG_SETTINGS_CLOSE  + System.getProperty("line.separator"));		
		bw.write(new String(builder));
		return true;
	}

	@Override
	public boolean loadElement(BufferedReader br) throws IOException {
		boolean cont = true;
		String line;
		String element;
		
		while (cont) {
        	line = br.readLine();
        	if (line.equalsIgnoreCase(TAG_SETTINGS_CLOSE))
        		cont = false;
        	else {
        		if (line.indexOf(TAG_NUM_ENTRIES) != -1) {
        			element = getElement(line);
        			if (element != null) {
        				setNumEntries(Integer.valueOf(element));
        			}
        		}
        		if (line.indexOf(TAG_ONLY_WIFI) != -1) {
        			element = getElement(line);
        			if (element != null) {
        				this.setOnlyWiFi(Boolean.valueOf(element));
        			}
        		}
        		if (line.indexOf(TAG_ONLY_WLAN) != -1) {
        			element = getElement(line);
        			if (element != null) {
        				this.setOnlyWLAN(Boolean.valueOf(element));
        			}
        		}
        	}
		}
		return true;
	}
}
