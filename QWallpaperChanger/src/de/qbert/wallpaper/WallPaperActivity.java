package de.qbert.wallpaper;

import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RemoteViews;
import android.widget.TimePicker;
import android.widget.Toast;
import de.qbert.wallpaper.alarm.WallpaperReceiver;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;
import de.qbert.wallpaper.view.FavoriteFragment;
import de.qbert.wallpaper.view.RecentFragment;
import de.qbert.wallpaper.view.ShowHelpActivity;
import de.qbert.wallpaper.view.TimeFragment;

public class WallPaperActivity extends ActionBarActivity implements ActionBar.TabListener {

	public static final String INTENT_UPDATE = "de.qbert.qwallpaper.Intent_Update";

	public static final int ID_SETTINGS = 1;

	// Notifications.
	// This Activity acts as a Controller in an MVC pattern.
	// These are the notifications the Fragments can sent to the Activity acting as the controller.
	public static final int NOTIFICATION_NEW_RECENT = 1;
	public static final int NOTIFICATION_NO_WALLPAPER_FOUND = 2;
	public static final int NOTIFICATION_RECENT_DELETE = 3;
	public static final int NOTIFICATION_FAVORITE_DELETE = 4;
	public static final int NOTIFICATION_RECENT_SHARE = 5;
	public static final int NOTIFICATION_FAVORITE_SHARE = 6;
	public static final int NOTIFICATION_RECENT_SET_WALLPAPER = 7;
	public static final int NOTIFICATION_FAVORITE_SET_WALLPAPER = 8;
	public static final int NOTIFICATION_RECENT_FAVORITE = 9;

	public static final int NOTIFICATION_NO_DATA = -1;

	public static int REQUEST_CODE_ALARM_INTENT = 8989;

	private static final String TAG="Wallpaper";

	private static Context context = null;

	private DocBase doc;

	private TimeFragment timeFragment = null;
	private RecentFragment recentFragment = null;
	private FavoriteFragment favoriteFragment = null;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	private SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;

	public DocBase getDoc(){
		return doc;
	}

	public static Context getAppContext() {
		return WallPaperActivity.context;
	}

	public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		WallPaperActivity.context = getApplicationContext();

		setContentView(R.layout.activity_wall_paper);
		
		 this.requestPermissions(
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);


		doc = DocBase.getInstance();
		if (doc.getLastError() != null){
			Toast.makeText(getBaseContext(), doc.getLastError(), Toast.LENGTH_LONG).show();
		} else {

			// Set up the action bar.
			final ActionBar actionBar = getSupportActionBar();
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

			// Create the adapter that will return a fragment for each of the three
			// primary sections of the activity.
			mSectionsPagerAdapter = new SectionsPagerAdapter(
					getSupportFragmentManager());

			// Set up the ViewPager with the sections adapter.
			mViewPager = (ViewPager) findViewById(R.id.pager);
			mViewPager.setAdapter(mSectionsPagerAdapter);

			// When swiping between different sections, select the corresponding
			// tab. We can also use ActionBar.Tab#select() to do this if we have
			// a reference to the Tab.
			mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					actionBar.setSelectedNavigationItem(position);
				}
			});

			// For each of the sections in the app, add a tab to the action bar.
			for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
				// Create a tab with text corresponding to the page title defined by
				// the adapter. Also specify this Activity object, which implements
				// the TabListener interface, as the callback (listener) for when
				// this tab is selected.
				actionBar.addTab(actionBar.newTab()
						.setText(mSectionsPagerAdapter.getPageTitle(i))
						.setTabListener(this));
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.wall_paper, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_help) {
			Intent iHelp = new Intent(this, ShowHelpActivity.class);
			startActivity(iHelp);
			return true;		
		}
		if (id == R.id.action_settings) {
			Intent iPrefs = new Intent(this, WallpaperPreferencesActivity.class);
			startActivityForResult(iPrefs, ID_SETTINGS);
			return true;
		}
		if (id == R.id.action_settings_update) {

			int maxPictures = doc.getTotal();
			if (maxPictures < 10) {
				maxPictures = 10;
			}
				
			if(maxPictures > 4000){
				maxPictures = 4000;
			}

			Random rand = new Random();
			int randPicture = rand.nextInt(maxPictures) + 1;

			SearchImage si = new SearchImage(this, doc);
			si.getNextImage(randPicture);

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		
		if ( (timeFragment != null) && (timeFragment.getView() != null) ) {
		 InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.hideSoftInputFromWindow(timeFragment.getView().getWindowToken(), 0);
		}
		    
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).			
			switch(position){
			case 0:
				timeFragment = TimeFragment.newInstance(position + 1);
				return timeFragment;
			case 1:
				recentFragment = RecentFragment.newInstance(position + 1);
				return recentFragment;
			case 2:
				favoriteFragment = FavoriteFragment.newInstance(position + 1);
				return favoriteFragment;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section3).toUpperCase(l);
			case 2:
				return getString(R.string.title_section4).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * On Button Click handler for all buttons
	 * @param v
	 */
	public void onBtnClicked(View v){
		Log.d(TAG, "id:" + v.getId());
		if(v.getId() == R.id.btnTagEdit){
			
			Intent intent = new Intent(this, TagEditActivity.class);
			startActivity(intent);
		}
		
		if(v.getId() == R.id.btnStartTimer){
			Toast.makeText(getBaseContext(), "Start alarm", Toast.LENGTH_LONG).show();
			TimePicker timePicker = timeFragment.getTimePicker();

			int hour = timePicker.getCurrentHour();
			int minute = timePicker.getCurrentMinute();

			DocBase doc = DocBase.getInstance();

			boolean daySet = timeFragment.getBtnMonday() || timeFragment.getBtnTuesday() || timeFragment.getBtnWednesday() ||
					timeFragment.getBtnThursday() ||timeFragment.getBtnFriday() || timeFragment.getBtnSaturday() ||
					timeFragment.getBtnSunday();

			if (daySet) {
				doc.setEvent(hour, minute,
						timeFragment.getBtnMonday(), timeFragment.getBtnTuesday(), timeFragment.getBtnWednesday(),
						timeFragment.getBtnThursday(), timeFragment.getBtnFriday(), timeFragment.getBtnSaturday(),
						timeFragment.getBtnSunday());

				setAlarm(hour, minute);
				doc.storeDocument();

				timeFragment.setButtonState();
			}	    
		}

		if(v.getId() == R.id.btnStopTimer){
			Toast.makeText(getBaseContext(), "Stop alarm", Toast.LENGTH_LONG).show();
			AlarmManager alarmMgr;
			PendingIntent alarmIntent;

			alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(this, WallpaperReceiver.class);
			alarmIntent = PendingIntent.getBroadcast(this, REQUEST_CODE_ALARM_INTENT, intent, 0);

			// Remove old intent
			alarmMgr.cancel(alarmIntent);			

			doc.setEventInActive();
			doc.storeDocument();

			timeFragment.setButtonState();
		}	

		if(v.getId() == R.id.btnNowTimer){
			TimePicker timePicker = timeFragment.getTimePicker();
			Calendar calendar = Calendar.getInstance();
			calendar.getTimeInMillis();
			timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
			timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
			Toast.makeText(getBaseContext(), "Set Time to now", Toast.LENGTH_LONG).show();
		}

		if( (v.getId() == R.id.btnMonday) || (v.getId() == R.id.btnTuesday) || (v.getId() == R.id.btnWednesday) ||
				(v.getId() == R.id.btnThursday) || (v.getId() == R.id.btnFriday) || (v.getId() == R.id.btnSaturday) ||
				(v.getId() == R.id.btnSunday) ){
			DocBase doc = DocBase.getInstance();
			int hour = doc.getEvent().getHour();
			int minute = doc.getEvent().getMinute();

			doc.setEvent(hour, minute,
					timeFragment.getBtnMonday(), timeFragment.getBtnTuesday(), timeFragment.getBtnWednesday(),
					timeFragment.getBtnThursday(), timeFragment.getBtnFriday(), timeFragment.getBtnSaturday(),
					timeFragment.getBtnSunday());
			doc.storeDocument();	    	
		}
	}

	// ----------------------------
	// Alarm functions
	// ----------------------------

	private void setAlarm(int hour, int minute){

		try {
			AlarmManager alarmMgr;
			PendingIntent alarmIntent;
	
			alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(this, WallpaperReceiver.class);
			intent.setData(Uri.parse("timer:" + "999"));
			intent.setAction(INTENT_UPDATE);
			alarmIntent = PendingIntent.getBroadcast(this, REQUEST_CODE_ALARM_INTENT, intent, 0);
	
			// Set the alarm to start at passed in time
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.set(Calendar.HOUR_OF_DAY, hour);
			calendar.set(Calendar.MINUTE, minute);
	
			// Remove old intent
			//alarmMgr.cancel(alarmIntent);
	
			// setRepeating() lets you specify a precise custom interval. In this case, 24 hours.
			alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
		} catch (Exception ex) {
			Log.d(TAG, "error:" + ex.toString());
            ex.printStackTrace();
            Toast.makeText(getBaseContext(), ex.toString(), Toast.LENGTH_LONG).show();
        }
	}

	@SuppressLint("NewApi")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == RESULT_OK) {

			if (requestCode == ID_SETTINGS) {
				// We store the settings in the document
				SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);

				String strRecentEntries = SP.getString("recent_entries", DocBase.MAX_RECENT_ENTRIES_DEF);
				doc.setSettingsNumEntries(Integer.valueOf(strRecentEntries));

				Boolean wifiOnly = SP.getBoolean("wifi_only", DocBase.MAX_WIFI_ONLY_DEF);
				doc.setOnlyWiFi(Boolean.valueOf(wifiOnly));

				Boolean WLanOnly = SP.getBoolean("charging_only", DocBase.MAX_WLAN_ONLY_DEF);
				doc.setOnlyWLan(Boolean.valueOf(WLanOnly));

				doc.storeDocument();
			}
		}
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if(cursor!=null)
		{
			//HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
			//THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		else return null;
	}

	/**
	 * NOTIFICATION function
	 * @param commmand
	 * @param data
	 */
	public void notifyChange(int commmand, int data){

		Log.d(TAG, "NOTIFICATION: cmd: " + commmand + ", data: " +data);

		if (commmand == NOTIFICATION_NEW_RECENT) {
			if (recentFragment != null) {
				recentFragment.notifyDataSetChanged();
			}
			Toast.makeText(getBaseContext(), getString(R.string.infoWallpaperSet), Toast.LENGTH_LONG).show();
		}

		if (commmand == NOTIFICATION_NO_WALLPAPER_FOUND) {
			Toast.makeText(getBaseContext(), getString(R.string.errorNoWallaperFound), Toast.LENGTH_LONG).show();
		}

		if (commmand == NOTIFICATION_RECENT_DELETE) {
			WallpaperEntry entry = doc.getRecentWallpaperList().get(data);			
			// Update document
			doc.deleteRecentWallpaper(entry.getUrl());

			// Update View
			if (recentFragment != null) {
				recentFragment.notifyDataSetChanged();
			}
			if (favoriteFragment != null) {
				favoriteFragment.notifyDataSetChanged();
			}

			// Make change persistent
			doc.storeDocument();
			Toast.makeText(this, getString(R.string.infoWallpaperDeleted) + entry.getTitle(), Toast.LENGTH_SHORT).show();
		}
		if (commmand == NOTIFICATION_FAVORITE_DELETE) {
			WallpaperEntry entry = doc.getFavoriteWallpaperList().get(data);
			// Update document
			doc.deleteFavoriteWallpaper(entry.getUrl());

			// Update View
			if (recentFragment != null) {
				recentFragment.notifyDataSetChanged();
			}
			if (favoriteFragment != null) {
				favoriteFragment.notifyDataSetChanged();
			}

			// Make change persistent
			doc.storeDocument();
			Toast.makeText(this, getString(R.string.infoWallpaperDeleted) + entry.getTitle(), Toast.LENGTH_SHORT).show();
		}
		if (commmand == NOTIFICATION_RECENT_SHARE) {
			WallpaperEntry entry = doc.getRecentWallpaperList().get(data);

			Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND); 
			sharingIntent.setType("image/jpeg");
			String shareBody = getString(R.string.infoSharedBy);
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, entry.getUrl());
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
			startActivity(Intent.createChooser(sharingIntent, getString(R.string.infoSharedVia)));
			Toast.makeText(this, getString(R.string.infoShare) + entry.getTitle(), Toast.LENGTH_SHORT).show();
		}
		if (commmand == NOTIFICATION_FAVORITE_SHARE) {
			WallpaperEntry entry = doc.getFavoriteWallpaperList().get(data);

			Intent sharingIntent = new Intent(Intent.ACTION_SEND);
			sharingIntent.setType("image/jpeg");
			String path = "file://" + doc.getDocStoreFolder() + "/" + entry.getFileName();
			sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path)); //Uri.parse("file:///sdcard/temporary_file.jpg"));
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, entry.getUrl());
			startActivity(Intent.createChooser(sharingIntent, getString(R.string.infoSharedVia)));	
			Toast.makeText(this, getString(R.string.infoShare) + entry.getTitle(), Toast.LENGTH_SHORT).show();
		}
		if (commmand == NOTIFICATION_RECENT_SET_WALLPAPER) {
			WallpaperEntry entry = doc.getRecentWallpaperList().get(data);
			String path = doc.getDocRecentFolder() + "/" + entry.getFileName();

			SearchImage si = new SearchImage(this, doc);
			si.setWallpaper(path);

			doc.setActiveWallpaper(entry);
			doc.storeDocument();

			if (recentFragment != null) {
				recentFragment.notifyDataSetChanged();
			}

			updateWidget(entry);

			Toast.makeText(this, getString(R.string.infoSetWallpaper) + entry.getTitle(), Toast.LENGTH_SHORT).show();
		}
		if (commmand == NOTIFICATION_FAVORITE_SET_WALLPAPER) {
			WallpaperEntry entry = doc.getFavoriteWallpaperList().get(data);
			String path = doc.getDocStoreFolder() + "/" + entry.getFileName();

			SearchImage si = new SearchImage(this, doc);
			si.setWallpaper(path);

			doc.setActiveWallpaper(entry);
			doc.storeDocument();

			if (favoriteFragment != null) {
				favoriteFragment.notifyDataSetChanged();
			}

			updateWidget(entry);

			Toast.makeText(this, getString(R.string.infoSetWallpaper) + entry.getTitle(), Toast.LENGTH_SHORT).show();
		}
		if (commmand == NOTIFICATION_RECENT_FAVORITE) {
			WallpaperEntry entry = doc.getRecentWallpaperList().get(data);
			doc.setFavorite(entry);
			doc.storeDocument();

			// Update View
			if (recentFragment != null) {
				recentFragment.notifyDataSetChanged();
			}
			if (favoriteFragment != null) {
				favoriteFragment.notifyDataSetChanged();
			}

			Toast.makeText(this, getString(R.string.infoSetFavorite) + entry.getTitle(), Toast.LENGTH_SHORT).show();
		}		
	}

	private void updateWidget(WallpaperEntry entry){
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);

		ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

		for (int widgetId : allWidgetIds) {
			RemoteViews remoteViews = new RemoteViews(context
					.getApplicationContext().getPackageName(),
					R.layout.widget_layout);
			String title = doc.getActiveWallpaperTitle();
			if (title == null)
				title = "--- nothing found ----";
			remoteViews.setTextViewText(R.id.update_info, title);

			if (doc.isFavorite(entry)){
				remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star_sel);
			} else {
				remoteViews.setImageViewResource(R.id.imgFavorite, R.drawable.ic_action_star);
			}

			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
	}
}
