package de.qbert.wallpaper;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import de.qbert.wallpaper.doc.DocBase;

public class WidgetProvider extends AppWidgetProvider {

	private static final String LOG_TAG = "Wallpaper Widget";

	public static final String ACTION_UPDATE_TAG = "de.qbert.wallpaper.ACTION_UPDATE_TAG";
	public static final String ACTION_UPDATE_CLICK = "de.qbert.wallpaper.ACTION_UPDATE_CLICK";
	public static final String ACTION_UPDATE_FORWARD = "de.qbert.wallpaper.ACTION_UPDATE_FORWARD";
	public static final String ACTION_UPDATE_BACKWARD = "de.qbert.wallpaper.ACTION_UPDATE_BACKWARD";
	public static final String ACTION_UPDATE_FAVORITE = "de.qbert.wallpaper.ACTION_UPDATE_FAVORITE";
	public static final String ACTION_UPDATE_TITLE = "de.qbert.wallpaper.ACTION_UPDATE_FAVORITE";
	public static final String ACTION_START_APP = "de.qbert.wallpaper.ACTION_START_APP";
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		Log.w(LOG_TAG, "onUpdate method called");

		// Get all ids
		ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

		Intent serviceIntent = new Intent(context.getApplicationContext(), WidgetProviderService.class);
		serviceIntent.putExtra(WidgetProviderService.SERVICE_ACTION_ID, WidgetProviderService.SERVICE_ACTION_UPDATENAME);
		serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
		context.startService(serviceIntent);
	}	

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.w(LOG_TAG, "onReceive method called");

		int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
		
	    if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			// Build the intent to call the service
			Intent serviceIntent = new Intent(context.getApplicationContext(), WidgetProviderService.class);
			serviceIntent.putExtra(WidgetProviderService.SERVICE_ACTION_ID, WidgetProviderService.SERVICE_ACTION_UPDATE);
			context.startService(serviceIntent);	    	
	    }


		if (ACTION_UPDATE_CLICK.equals(intent.getAction())) {			
			// Build the intent to call the service
			Intent serviceIntent = new Intent(context.getApplicationContext(), WidgetProviderService.class);
			serviceIntent.putExtra(WidgetProviderService.SERVICE_ACTION_ID, WidgetProviderService.SERVICE_ACTION_UPDATE);
			context.startService(serviceIntent);
		}

		if (ACTION_UPDATE_FORWARD.equals(intent.getAction())) {
			// Build the intent to call the service
			Intent serviceIntent = new Intent(context.getApplicationContext(), WidgetProviderService.class);
			serviceIntent.putExtra(WidgetProviderService.SERVICE_ACTION_ID, WidgetProviderService.SERVICE_ACTION_FORWARD);
			context.startService(serviceIntent);
		}

		if (ACTION_UPDATE_BACKWARD.equals(intent.getAction())) {
			// Build the intent to call the service
			Intent serviceIntent = new Intent(context.getApplicationContext(), WidgetProviderService.class);
			serviceIntent.putExtra(WidgetProviderService.SERVICE_ACTION_ID, WidgetProviderService.SERVICE_ACTION_BACKWARD);
			context.startService(serviceIntent);
		}

		if (ACTION_UPDATE_FAVORITE.equals(intent.getAction())) {
			// Build the intent to call the service
			Intent serviceIntent = new Intent(context.getApplicationContext(), WidgetProviderService.class);
			serviceIntent.putExtra(WidgetProviderService.SERVICE_ACTION_ID, WidgetProviderService.SERVICE_ACTION_FAVORITE);
			context.startService(serviceIntent);
		}
		
		if (ACTION_START_APP.equals(intent.getAction())) {
			//Toast.makeText(context, "Start App", Toast.LENGTH_LONG).show();
			Intent myIntent = new Intent(context.getApplicationContext(), WallPaperActivity.class);
			myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			//myIntent.putExtra("key", value); //Optional parameters
			context.getApplicationContext().startActivity(myIntent);			
		}

		if (ACTION_UPDATE_TITLE.equals(intent.getAction())) {
			DocBase doc = DocBase.getInstance();
			if (doc.getLastError() != null){
				Toast.makeText(context, doc.getLastError(), Toast.LENGTH_LONG).show();
			} else {
				for (int widgetId : allWidgetIds) {
					RemoteViews remoteViews = new RemoteViews(context
							.getApplicationContext().getPackageName(),
							R.layout.widget_layout);
					String title = doc.getActiveWallpaperTitle();
					if (title == null)
						title = "---";
					remoteViews.setTextViewText(R.id.update_info, title);

					AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext());
					appWidgetManager.updateAppWidget(widgetId, remoteViews);
				}
			}
		}

		super.onReceive(context, intent);
	}


	protected PendingIntent getPendingSelfIntent(Context context, String action) {
		Intent intent = new Intent(context, getClass());
		intent.setAction(action);
		return PendingIntent.getBroadcast(context, 0, intent, 0);
	}

}
