package de.qbert.wallpaper;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntrySearch.SearchEntry;

public class TagEditActivity extends ActionBarActivity implements OnClickListener {
	
	private static final String TAG = "Wallpaper";
	
	public static final int NOTIFICATION_CONTEXT_DELETE = 1;
	public static final int NOTIFICATION_CONTEXT_SELECT = 2;
	
	private int mPosition = -1;
	
	private PlaceholderFragment myFragment = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tag_edit);
		
		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		//actionBar.setDisplayOptions(ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
		actionBar.setTitle(R.string.tag_edit_title);
		actionBar.setHomeButtonEnabled(true);
		
		if (savedInstanceState == null) {
			myFragment = new PlaceholderFragment();
			getSupportFragmentManager().beginTransaction().add(R.id.container, myFragment).commit();
		}
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.tag_edit, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		Log.d(TAG, "item selected: " +id);
		
		if (id == R.id.action_settings) {
			return true;
		}
		if (id ==  android.R.id.home) {
			Log.d(TAG, "item selected: home");
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends ListFragment {

		public PlaceholderFragment() {
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
			List<SearchEntry> list = getDoc().getTagList();
			
			if (list.size() == 0){

			} else {
				Log.d(TAG, "num entries:" + list.size());
				
				MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), list);
		    	setListAdapter(adapter);
			}
		}
		
		private DocBase getDoc(){
			return DocBase.getInstance();
		}
		
		public void notifyDataSetChanged(){
			List<SearchEntry> list = getDoc().getTagList();
			MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), list);
			setListAdapter(adapter);
		}

		
		//--------------------------------------------------------------
		//  Custom Adapter
		//--------------------------------------------------------------

		public class MySimpleArrayAdapter extends ArrayAdapter<SearchEntry> {
			  private final Context context;
			  private final List<SearchEntry> values;

			  public MySimpleArrayAdapter(Context context,  List<SearchEntry> values) {
			    super(context, -1, values);
			    this.context = context;
			    this.values = values;
			  }

			  @Override
			  public View getView(int position, View convertView, ViewGroup parent) {
			    LayoutInflater inflater = (LayoutInflater) context
			        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    View rowView = inflater.inflate(R.layout.recent_row, parent, false);
			    
			    TextView textViewTitle = (TextView) rowView.findViewById(R.id.title);
			    TextView textViewUrl = (TextView) rowView.findViewById(R.id.Url);
			    TextView textViewTime = (TextView) rowView.findViewById(R.id.Time);
			    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
			    ImageView imageViewFavorite = (ImageView) rowView.findViewById(R.id.imgFavorite);
			    Button btnContextMenu = (Button) rowView.findViewById(R.id.btnContextMenu);

			    textViewTitle.setText(values.get(position).getSearchText());
			    textViewUrl.setText("---");
			    textViewTime.setText("---");
			    
				if (getDoc().isActiveSearchText(position)) {
					imageViewFavorite.setBackgroundResource(R.drawable.ic_action_star_dark);
				} else {
					imageViewFavorite.setVisibility(View.GONE);
				}
			
			    new setTagIcon().execute(values.get(position).getSearchText(), imageView);			    
				
			    // Install onClick listener for the item popup menu
			    // and store the position of this entry in the view tag.
			    btnContextMenu.setTag(position);
				btnContextMenu.setOnClickListener((TagEditActivity)getActivity());
			    
			    return rowView;
			  }
			} 


		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.list_view, container, false);
		}

		// Handle clicks on list items
		@Override
		public void onListItemClick(ListView l, View v, int position, long id) {
			DocBase doc =  DocBase.getInstance();
			
			// Update document
			doc.setActive(position);

			notifyDataSetChanged();

			// Make change persistent
			doc.storeDocument();
			super.onListItemClick(l, v, position, id);
		}
		
		private class setTagIcon extends AsyncTask<Object,Void,Bitmap>{

	        ImageView wallpaperIcon = null;

	        @Override
	        protected Bitmap doInBackground(Object...params) {
	            try{
	            	Log.d(TAG,  "loading tags: " + (String)params[0]);
	            	
	            	DocBase doc = DocBase.getInstance();
	            	String[] tags = doc.geFourtWallpaperByTag((String)params[0]);
	            	
	            	for (int i=0; i<tags.length; i++) {
	            		Log.d(TAG, "file: " + tags[i]);
	            	}
	            	
//	                Bitmap eventImage = decodeFile((String) params[0]);
//	                wallpaperIcon =  (ImageView) params[1];
	                return null;
	            }
	            catch(Exception e){
	                e.printStackTrace();
	                return null;
	            }
	        }

	        @Override
	        protected void onPostExecute(Bitmap eventImage) {
	            if(eventImage != null && wallpaperIcon != null){
	            	wallpaperIcon.setImageBitmap(eventImage);
	            }
	        }  

		}
	}  // End ListFragment

	// Handle clicks on menu icon of each list entry
	@Override
	public void onClick(View view) {
    	//Toast.makeText(this, "clicked", Toast.LENGTH_LONG).show();
    	startPopupMenu((Integer)view.getTag(), view);
		
	}
		
	//---------------------------------
    // Context Menu for each list item
    //---------------------------------
    
	public void startPopupMenu(int position, View view){
	    PopupMenu popup = new PopupMenu(this, view);
	    
	    mPosition = position;
	    MenuInflater inflater = popup.getMenuInflater();
	    inflater.inflate(R.menu.tag_edit_menu, popup.getMenu());
	    
	    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
	        public boolean onMenuItemClick(MenuItem item) {
	            switch (item.getItemId()) {  
		        case R.id.tag_context_delete:
		        	//Toast.makeText(getApplicationContext(), "delete", Toast.LENGTH_LONG).show();
					notifyChange(TagEditActivity.NOTIFICATION_CONTEXT_DELETE, mPosition);		        	
		        	break;
		        case R.id.tag_context_select:
		        	//Toast.makeText(getApplicationContext(), "delete", Toast.LENGTH_LONG).show();
		        	notifyChange(TagEditActivity.NOTIFICATION_CONTEXT_SELECT, mPosition);		        	
		        	break;
		        	
		        default:
		        	// Nothing to do
		            break; 

	            }
	            mPosition = -1;
	            return true;
	        }
	    });
	    
	    popup.show();	
	}
	
	public void notifyChange(int commmand, int position){

		Log.d(TAG, "NOTIFICATION: cmd: " + commmand + ", data: " + position);
		
		if (commmand == NOTIFICATION_CONTEXT_DELETE) {
			DocBase doc =  DocBase.getInstance();
		
			// Update document
			doc.deleteSearchText(position);

			if (myFragment != null) {
				myFragment.notifyDataSetChanged();
			}

			// Make change persistent
			doc.storeDocument();
		}
		
		if (commmand == NOTIFICATION_CONTEXT_SELECT) {
			//Log.d(TAG, "item clicked: " + position);
			//Toast.makeText((TagEditActivity)getActivity().getApplicationContext(), "item", Toast.LENGTH_LONG).show();
			DocBase doc =  DocBase.getInstance();
			
			// Update document
			doc.setActive(position);

			if (myFragment != null) {
				myFragment.notifyDataSetChanged();
			}

			// Make change persistent
			doc.storeDocument();
		}
	}
}
