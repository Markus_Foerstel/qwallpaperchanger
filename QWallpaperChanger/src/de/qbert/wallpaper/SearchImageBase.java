package de.qbert.wallpaper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import de.qbert.wallpaper.doc.DocBase;
import de.qbert.wallpaper.doc.DocEntryWallpaper.WallpaperEntry;

public class SearchImageBase {
	
	public static final String TAG="QWallpaper";
	public static final String TAG_ERR="QWallpaper_ERR";

	public DocBase doc;
	public Context context;
	
	public SearchImageBase(Context context, DocBase doc) {
		super();
		this.context = context;
		this.doc = doc;
	}
	
	public void DownloadFromUrl(String imageURL, String fileName) {
		try {
			URL url = new URL(imageURL);
			File file = new File(fileName);

			long startTime = System.currentTimeMillis();
			Log.d(TAG, "download begining");
			Log.d(TAG, "download url:" + url);
			Log.d(TAG, "downloaded file name:" + fileName);
			/* Open a connection to that URL. */
			URLConnection ucon = url.openConnection();

			/*
			 * Define InputStreams to read from the URLConnection.
			 */
			InputStream is = ucon.getInputStream();

			/*
			 * Read bytes to the Buffer until there is nothing more to read(-1).
			 */
		    try {
			      FileOutputStream fos = new FileOutputStream(file);
			      int i = is.read();
			      while(i != -1) {
			        fos.write(i);
			        i = is.read();
			      }
			      fos.close();
		    } catch (IOException e) {
		    	Log.d(TAG, e.getMessage());
		    }
		    			
			Log.d("ImageManager", "download ready in"
					+ ((System.currentTimeMillis() - startTime) / 1000)
					+ " sec");

		} catch (IOException e) {
			Log.d("ImageManager", "Error: " + e);
		}
	}
	
	public WallpaperEntry setNextImage(boolean forward){
		WallpaperEntry entry = doc.setNextImage(!forward);
		
		String path = doc.getDocRecentFolder() + "/" + entry.getFileName();
		setWallpaper(path);
		
		return entry;
	}
	
	public void setWallpaper(String path){

		WallpaperManager wallpaperMgr = WallpaperManager.getInstance(context); 
		int width = wallpaperMgr.getDesiredMinimumWidth();
		int height = wallpaperMgr.getDesiredMinimumHeight();
		
		if (((width <= 0)) || (height <= 0)) {

			DisplayMetrics metrics = new DisplayMetrics(); 
			WindowManager winMgr = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
			winMgr.getDefaultDisplay().getMetrics(metrics);
			
		    width = metrics.widthPixels;
		    height = metrics.heightPixels;
		}

		// get the Image as Bitmap 
		Bitmap bitmap = decodeFile(path, width, height);
		
		int bmpWidth = bitmap.getWidth();
		int bmpHeight = bitmap.getHeight();
		
		Rect rect = new Rect();
		int top = 0;
		int left = 0;
		int bottom = bmpHeight;
		int right = bmpWidth;
		
		if (bmpWidth > width) {
			left = (bmpWidth - width)/2;
			right = left + width;
		}
		
		if (bmpHeight > height) {	
			top = (bmpHeight - height)/2;
			bottom = top + height;			
		}
			
		rect.set(left, top, right, bottom);
		
		try {
			wallpaperMgr.setWallpaperOffsetSteps(0.5f, 0.0f);
			wallpaperMgr.setBitmap(bitmap, rect, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (bitmap != null) {
			bitmap.recycle();
		}
	}
	
	//decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(String path, int width, int height) {
		try {
			File f = new File(path);
			//decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);

			//Find the correct scale value. It should be the power of 2.
			final int REQUIRED_WIDTH=width;
			final int REQUIRED_HEIGHT=height;
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			while(true){
				if((width_tmp/2 < REQUIRED_WIDTH) && (height_tmp/2 < REQUIRED_HEIGHT))
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale*=2;
			}
			Log.d(TAG, "WallpaperSize width: " + width);
			Log.d(TAG, "WallpaperSize height: " + height);
			Log.d(TAG, "WallpaperSize scale: " + scale);

			//decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			Log.d(TAG, "File not found: " + e.getMessage());
		}
		return null;
	}

}
